#include "stdafx.h"
#include "VBO.h"


E::VBO::VBO()
{

}

E::VBO::VBO(GLuint _location, int _dataSize, GLenum _type) : location(_location), dataSize(_dataSize), dataType(_type)
{
	glGenBuffers(1, &id);
}


void E::VBO::BindToVAO()
{
	glBindBuffer(GL_ARRAY_BUFFER, id);
	switch (dataType)
	{
	case GL_FLOAT:
		glVertexAttribPointer(location, dataSize, GL_FLOAT, GL_FALSE, 0, 0);
		break;
	case GL_INT:
		glVertexAttribIPointer(location, dataSize, GL_INT, 0, 0);
		break;
	case GL_DOUBLE:
		glVertexAttribLPointer(location, dataSize, GL_DOUBLE, 0, 0);
		break;
	default:
		break;
	}
	glEnableVertexAttribArray(location);
}


E::VBO::~VBO()
{
}
