#include "stdafx.h"
#include "Cube.h"

static float staticVerticesFrontFace[Cube::numberOfVerticesPerCubeFace]
{
	-0.5f,  0.5f, -0.5f	,	-0.5f,  -0.5f, -0.5f	,	 0.5f, -0.5f, -0.5f	,
	 0.5f, -0.5f, -0.5f	,	 0.5f,   0.5f, -0.5f	,	-0.5f,  0.5f, -0.5f
};
static float staticVerticesRightFace[Cube::numberOfVerticesPerCubeFace]
{
	0.5f,  0.5f, -0.5f	,	0.5f,  -0.5f, -0.5f		,	0.5f, -0.5f,  0.5f	,
	0.5f, -0.5f,  0.5f	,	0.5f,   0.5f,  0.5f		,	0.5f,  0.5f, -0.5f
};

static float staticVerticesBackFace[Cube::numberOfVerticesPerCubeFace]
{
	 0.5f,  0.5f, 0.5f	,	 0.5f,  -0.5f, 0.5f	,	-0.5f, -0.5f, 0.5f	,
	-0.5f, -0.5f, 0.5f	,	-0.5f,   0.5f, 0.5f	,	 0.5f,  0.5f, 0.5f
};

static float staticVerticesLeftFace[Cube::numberOfVerticesPerCubeFace]
{
	-0.5f,  0.5f,  0.5f	,	-0.5f,  -0.5f,  0.5f	,	-0.5f, -0.5f, -0.5f	,
	-0.5f, -0.5f, -0.5f	,	-0.5f,   0.5f, -0.5f	,	-0.5f,  0.5f,  0.5f
};

static float staticVerticesUpFace[Cube::numberOfVerticesPerCubeFace]
{
	-0.5f, 0.5f,  0.5f	,	-0.5f,  0.5f, -0.5f	,	 0.5f, 0.5f, -0.5f	,
	 0.5f, 0.5f, -0.5f	,	 0.5f,  0.5f,  0.5f	,	-0.5f, 0.5f,  0.5f
};

static float staticVerticesDownFace[Cube::numberOfVerticesPerCubeFace]
{
	0.5f, -0.5f, -0.5f	,	 0.5f,  -0.5f,  0.5f	,	-0.5f, -0.5f, 0.5f	,
	-0.5f, -0.5f, 0.5f	,	-0.5f,  -0.5f, -0.5f	,	0.5f, -0.5f, -0.5f	
};


static float * staticVerticesOriginCentered[Cube::numberOfFaceInCube] = 
{
	staticVerticesFrontFace, 
	staticVerticesRightFace, 
	staticVerticesBackFace, 
	staticVerticesLeftFace, 
	staticVerticesUpFace, 
	staticVerticesDownFace
};


static float staticTexCoords[12]
{
	0.0f, 0.0f	,	0.0f, 1.0f	,	1.0f, 1.0f	,
	1.0f, 1.0f	,	1.0f, 0.0f	,	0.0f, 0.0f
};


Cube::Cube()
{

}

Cube::Cube(glm::vec3 & pos, glm::vec3 & angle, glm::vec3 & scaling, bool createModel) : GameObject(pos, angle, scaling)
{
	if (createModel)
	{
		CreateModel();
	}
}


Cube::~Cube()
{
}

void Cube::Draw()
{
	PreDraw();
	for (int i = 0; i < allModels->size(); i++)
	{
		glBindVertexArray(allModels->at(i)->vaoID);
		glDrawArraysInstanced(GL_TRIANGLES, 0, 6, 1);
	}
}

void Cube::CreateModel()
{
	E::VBO * texCoordsVBO = Model::StaticGenVBO(staticTexCoords, Cube::numberOfTexCoordsPerCubeFace, GL_STATIC_DRAW, 1, 2, false);

	modelMatrixVBO = Model::StaticGenMatVBO(glm::value_ptr(transformation.modelMatrix), 16, 2, 4, GL_STATIC_DRAW, 4, sizeof(float), sizeof(glm::mat4), true);
	
	allModels = new std::vector<Model*>;
	for (int i = 0; i < Cube::numberOfFaceInCube; i++)
	{
		Model * model = new Model();
		model->GenVAO();
		model->GenVBO(staticVerticesOriginCentered[i], Cube::numberOfVerticesPerCubeFace, GL_STATIC_DRAW, 0, 3, false);
		model->numberOfVertex = Cube::numberOfVerticesPerCubeFace / 3;
		model->AddVBO(texCoordsVBO);
		model->AddVBO(modelMatrixVBO);
		model->BindAllVBO();
		allModels->push_back(model);
	}

	modelIsCreated = true;
}


