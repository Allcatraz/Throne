#include "stdafx.h"
#include "StandardShader.h"


StandardShader::StandardShader()
{

}

StandardShader::StandardShader(E::Shader * shader) : Shader(*shader)
{
	lightPositionLocation = glGetUniformLocation(programID, "lightPosition");

	lightColorLocation = glGetUniformLocation(programID, "lightColor");
	shineDamperLocation = glGetUniformLocation(programID, "shineDamper");
	reflectivityLocation = glGetUniformLocation(programID, "reflectivity");
	ambientLightingLocation = glGetUniformLocation(programID, "ambientLighting");
}






StandardShader::~StandardShader()
{
}

void StandardShader::SetLightPosition(glm::vec3 & lightPosition)
{
	SetVec3Float(lightPositionLocation, lightPosition);
}

void StandardShader::SetLightColor(glm::vec3 & lightColor)
{
	SetVec3Float(lightColorLocation, lightColor);
}

void StandardShader::SetShineDamper(float shineDamper)
{
	SetFloat(shineDamperLocation, shineDamper);
}

void StandardShader::SetReflectivity(float reflectivity)
{
	SetFloat(reflectivityLocation, reflectivity);
}

void StandardShader::SetAmbientLighting(float ambientLighting)
{
	SetFloat(ambientLightingLocation, ambientLighting);
}

