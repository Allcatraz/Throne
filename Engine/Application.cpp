#include "stdafx.h"
#include "Application.h"
#include <SFML/Graphics.hpp>
#include <string>
#include "OBJModel.h"
#include <unordered_map>
#include "ShaderManager.h"
#include "RenderedObject.h"


using namespace sf;
using namespace std;
using namespace glm;

/// <summary>
/// Initializes a new instance of the <see cref="Application" /> class.
/// </summary>
/// <param name="tailleWindowX">The taille window x.</param>
/// <param name="tailleWindowY">The taille window y.</param>
/// <param name="titre">The titre.</param>
///
Application::Application(unsigned int tailleWindowX, unsigned int tailleWindowY, std::string titre)
{
	srand(time(NULL));
	rand();

	sf::ContextSettings settings;
	settings.depthBits = 24;
	settings.stencilBits = 8;
	settings.antialiasingLevel = 4;
	settings.majorVersion = 4;
	settings.minorVersion = 5;

	
	window.create(VideoMode(tailleWindowX, tailleWindowY), titre, sf::Style::Default, settings);
	Mouse::setPosition(Vector2i(TAILLE_WINDOW_X / 2, TAILLE_WINDOW_Y / 2), window);


	glewInit();
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glEnable(GL_TEXTURE_2D);
	//glEnable(GL_CULL_FACE);
	//glCullFace(GL_BACK);
	glClearColor(1.0, 1.0, 0.0, 1.0);
	

	//shaderParticles = LoadShaderOpenGL("Shaders/ParticulesBase/Vertex_Base", "Shaders/ParticulesBase/Frag_Base");
	//shaderWorld = LoadShaderOpenGL("Shaders/World/Vertex_World", "Shaders/World/Frag_World");
	//shaderCube = LoadShaderOpenGL("Shaders/3D Geometry/Cube/Cube_Vert", "Shaders/3D Geometry/Cube/Cube_Frag");
	//shaderCube = LoadShaderOpenGL("Shaders/3D Geometry/Cube/Cube_VertInstanced2", "Shaders/3D Geometry/Cube/Cube_Frag");
	//shaderWorld = LoadShaderOpenGL("Shaders/WorldInstanced/Vertex_WorldInstanced", "Shaders/WorldInstanced/Frag_WorldInstanced");

	//cubeAssembly = new CubeAssembly();
	//cubeAssembly->shaderID = shaderCube->programID;
	//cubeAssembly->SetInstanced4();
	//cubeAssembly->actualPosition = glm::vec3(64, 150, 64);
	std::cout << "Erreur avant Scene() dans Application : " << glGetError() << endl;
	scene = new Scene();
	std::cout << "Erreur apr�s Scene() dans Application : " << glGetError() << endl;
	//scene->plane->shaderID = LoadShaderOpenGL("Shaders/3D Geometry/Plane/Plane_Vert", "Shaders/3D Geometry/Plane/Plane_Frag")->programID;

	//container = new ParticlesContainer(shaderParticles->programID, string("Textures/ParticleAtlasFire6.png"), 8, 8, vec3(3, 0, 1), GL_SRC_ALPHA_SATURATE, GL_ONE, 0.010f, 150, 15);
	//container = new ParticlesContainer(shaderParticles->programID, string("Textures/ParticleAtlasFire6.png"), 8, 8, vec3(3, 0, 1), GL_SRC_ALPHA, GL_ONE, 0.010f, 150, 15);

	
	//world = new World(shaderWorld->programID, false);

	std::cout << "Erreur avant glm::perspective dans Application : " << glGetError() << endl;
	projection = glm::perspective((double)Utility::ConvertirDegreeEnRadian(40.0), (double)tailleWindowX / tailleWindowY, 0.01, 1000.0);
	std::cout << "Erreur apr�s glm::perspective dans Application : " << glGetError() << endl;
	ShaderManager::SetProjectionMatrix(glm::value_ptr(projection));
}


int Application::Run()
{
	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		std::cout << "Erreur Avant boucle : " << error << endl;
	}
	try
	{
 		while (window.isOpen())
		{
			ManageInput();

			Update();
		
			Draw();
		}
	}
	catch (const std::exception& e)
	{
		cout << e.what() << endl;
		system("pause");
	}
	
	return EXIT_SUCCESS;
}

void Application::ManageInput()
{
	Event event;
	while (window.pollEvent(event))
	{
		switch (event.type)
		{
			// Lorsque la fen�tre est ferm�e
		case sf::Event::Closed:
			window.close();
			break;
			// Lorsqu'une touche du clavier est appuy�e
		case sf::Event::KeyReleased:
			switch (event.key.code)
			{
			case Keyboard::Escape:
			{
				window.close();
				if (nombreUpdate == 0)
				{
					cout << "Moyenne : " << 0 << "\n";
				}
				else
				{
					cout << "Moyenne : " << totalFrame / nombreUpdate << "\n";
				}
				scene->AfficherTempsMoyenParDrawCall();
				system("pause");
				break;
			}
			case Keyboard::LControl:
			{
				cameraActivated = !cameraActivated;
				break;
			}
			case Keyboard::Up:
			{
				camera.speed = 60;
				break;
			}
			case Keyboard::Down:
			{
				camera.speed = 6;
				break;
			}
			case Keyboard::F1:
			{
				camera.speed = 0.1f;
				break;
			}
			default:
				break;
			}
			break;
		case sf::Event::MouseMoved:
		{
			if (cameraActivated)
			{
				camera.moveOrientation(event.mouseMove.x - (TAILLE_WINDOW_X / 2), event.mouseMove.y - (TAILLE_WINDOW_Y / 2));			
			}	
			break;
		}
		default:
			break;
		}
	}
}

void Application::Draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	ShaderManager::SetViewMatrix(glm::value_ptr(view));
	

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//world->Draw();

	//cubeAssembly->DrawInstanced4();


	/*glDepthMask(GL_FALSE);
	container->Draw();
	glDepthMask(GL_TRUE);*/
	
	scene->Draw();

	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		std::cout << "Erreur Draw : " << error << endl;
	}
	

	window.display();
}

void Application::Update()
{
	gameTime = clock.restart().asSeconds();

	if (gameTime >= 0.5f)
	{
		gameTime = 0.01f;
	}

	FPSCounter();

	if (cameraActivated)
	{
		Mouse::setPosition(Vector2i(TAILLE_WINDOW_X / 2, TAILLE_WINDOW_Y / 2), window);
	}	

	//cubeAssembly->Update(camera.GetPosition());

	scene->Update(gameTime);

	GererCamera();

	//container->Update(gameTime, camera.GetPosition(), camera.GetViewPoint());
}

inline void Application::FPSCounter()
{
	frames++;
	timeSinceLastFpsCount += gameTime;
	if (timeSinceLastFpsCount >= 1)
	{
		cout << "FPS : " << frames << "\n";
		nombreUpdate++;
		totalFrame += frames;
		timeSinceLastFpsCount = 0;
		frames = 0;
	}
}

inline void Application::GererCamera()
{
	camera.lookAt(view);
	camera.move(gameTime);
}


Application::~Application()
{
	//delete container;
	//delete shaderParticles;
	//delete shaderWorld;
	//delete world;
}
