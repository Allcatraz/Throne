#pragma once
namespace E
{
	class Texture
	{
		
	public:
		Texture();
		Texture(const std::string& path);
		Texture(sf::Image& image);
		~Texture();

		void BindTextureToShader(const std::string& attributeName, GLuint shaderProgramID, int index);

		void LoadWithImagePath(const std::string& path);
		void LoadWithImage(sf::Image& image);
	
		static GLuint LoadTextureWithPath(const std::string& path);
		static GLuint LoadTextureWithImage(sf::Image& image);

		static void BindTextureArrayToShader(const std::string& attributeName, GLuint shaderProgramID, std::vector<Texture>& texturesToBind);
		static void BindTextureToShader(const std::string& attributeName, GLuint shaderProgramID, GLuint textureHandle, int index);

	
		GLuint handle;
	};

}


