#include "stdafx.h"
#include "Plane.h"


static float staticVertices[18]
{
	-0.5f, 0.0f,  0.5f	,	-0.5f,  0.0f, -0.5f	,	 0.5f, 0.0f, -0.5f	,
	 0.5f, 0.0f, -0.5f	,	 0.5f,  0.0f,  0.5f	,	-0.5f, 0.0f,  0.5f
};

static float staticTexCoords[12]
{
	0.0f, 0.0f	,	0.0f, 1.0f	,	1.0f, 1.0f	,
	1.0f, 1.0f	,	1.0f, 0.0f	,	0.0f, 0.0f
};



Plane::Plane()
{
}

Plane::Plane(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale, bool createModel) : GameObject(position, rotation, scale)
{
	if (createModel)
	{
		CreateModel();
	}
}


Plane::~Plane()
{
}

void Plane::Draw()
{
	PreDraw();
	glBindVertexArray(allModels->at(0)->vaoID);
	glDrawArraysInstanced(GL_TRIANGLES, 0, 6, 1);
}

void Plane::CreateModel()
{
	E::VBO * texCoordsVBO = Model::StaticGenVBO(staticTexCoords, 12, GL_STATIC_DRAW, 1, 2, false);

	modelMatrixVBO = Model::StaticGenMatVBO(glm::value_ptr(transformation.modelMatrix), 16, 2, 4, GL_STREAM_DRAW, 4, sizeof(float), sizeof(glm::mat4), true);

	allModels = new std::vector<Model*>;
	Model * model = new Model();
	model->GenVAO();
	model->GenVBO(staticVertices, 18, GL_STATIC_DRAW, 0, 3, false);
	model->numberOfVertex = 18 / 3;
	model->AddVBO(texCoordsVBO);
	model->AddVBO(modelMatrixVBO);
	model->BindAllVBO();
	allModels->push_back(model);

	modelIsCreated = true;
}
