#pragma once
#include "Model.h"
#include <string.h>
#include "Light.h"
#include "Transformation.h"
#include "Texture.h"




class OBJModel
{
public:
	OBJModel();
	~OBJModel();

	std::vector<Model*> models;


	void LoadOBJ(std::string & filePath);

	static bool StaticLoadOBJ(std::string & filePath, OBJModel* objModel);



};

