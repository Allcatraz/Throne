#pragma once
#include "Model.h"
#include "CubeData.h"
#include <vector>
#include <list>
#include "TemporaryDA.h"
#include "Texture.h"

class CubeWorld
{
public:
	CubeWorld();
	~CubeWorld();

	void SetMesh();
	void DrawMesh();

	void SetInstanced();
	void DrawInstanced();
	
	void SetInstanced4();
	void DrawInstanced4();

	void SetFacesToRender(int minX, int maxX, int minY, int maxY, int minZ, int maxZ);

	std::vector<Model*> allModels;
	std::list<std::vector<float>> vectorList;

	Model * model;
	E::Texture texture;
	GLuint shaderID;

	const int sizeX = 128;
	const int sizeY = 256;
	const int sizeZ = 128;
	const int cubeCount = sizeX * sizeY * sizeZ;

	int numberOfFacesToDraw[CubeData::numberOfFaceInCube] = {0, 0, 0, 0, 0, 0};

	E::VBO * facesPositionsVBO[CubeData::numberOfFaceInCube];
	TemporaryDA allFaces[CubeData::numberOfFaceInCube];
	CubeData *** cubes;

	glm::vec3 actualPosition;



	int numberOfBlockDifferenceBeforeUpdate = 1;
	void Update(glm::vec3& newPosition);

};

