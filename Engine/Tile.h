#pragma once
class Tile
{
public:
	Tile();
	~Tile();
	float vertices[18];
	float textureCoords[12];
	int textureIndex;
	int textureIndexLine1[18];
	int textureIndexLine2[18];
	int textureIndexLine3[18];
	static Tile Create(float x, float z, float size, float res, float multiplier);
};

