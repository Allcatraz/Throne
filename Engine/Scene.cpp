#include "stdafx.h"
#include "Scene.h"
#include "ShaderManager.h"


Scene::Scene()
{
	Creation();
}


Scene::~Scene()
{

}

void Scene::Draw()
{
	//sphere->Draw();

	world->Draw();

	sf::Clock clock;
	clock.restart();

	glEnable(GL_CULL_FACE);
	//treeTwig2->Draw();
	//treeBark2->Draw();
	//treeTrunk2->Draw();
	//treeTwig1->Draw();
	//treeRoots2->Draw();

	//tankRenderer1->Draw();
	//tankRenderer2->Draw();
	//tankRenderer3->Draw();
	//tankRenderer4->Draw();
	//tankRenderer5->Draw();
	glDisable(GL_CULL_FACE);

	//treeBranch3->Draw();
	//treeBud1->Draw();
	//treeBranch4->Draw();
	//treeBranch7->Draw();

	//glFinish();


	grass2Renderer->Draw();

	float elapsedTime = clock.getElapsedTime().asSeconds();
	totalTimeInDraw += elapsedTime;
	numberOfDrawCall++;
}

void Scene::Creation()
{
	ambientLighting = 0.5f;

	light.position = glm::vec3(0, 20, 0);
	light.color = glm::vec3(1.0f);

	standardShader = new StandardShader(ShaderManager::LoadShaderOpenGL("Shaders/Standard/Vert_Model_Texture", "Shaders/Standard/Frag_Model_Texture"));

	standardShader->SetAmbientLighting(ambientLighting);
	standardShader->SetShineDamper(10.0f);
	standardShader->SetReflectivity(0.0f);
	standardShader->SetLightPosition(light.position);
	standardShader->SetLightColor(light.color);

	E::Shader * shaderWorld = ShaderManager::LoadShaderOpenGL("Shaders/Old/World/Vertex_World", "Shaders/Old/World/Frag_World");
	world = new World(shaderWorld->programID, false);

#pragma region Geometry

	//cube = new Cube(glm::vec3(0, 0, 0), glm::vec3(), glm::vec3(1, 1, 1), true);
	//E::Texture * cubeTexture = new E::Texture();
	//cubeTexture->LoadWithImagePath("Textures/Grass.png");
	//cube->texture = cubeTexture;
	//cube->shader = ShaderManager::LoadShaderOpenGL("Shaders/Old/3D Geometry/Cube/Cube_Vert_v1", "Shaders/Old/3D Geometry/Cube/Cube_Frag");

	/*
	sphere = new Sphere(glm::vec3(), glm::vec3(), glm::vec3(1, 1, 1), 1, true);
	sphere->shaderID = shader->programID;
	*/









#pragma endregion

#pragma region Tree

#pragma region Tree textures	
	E::Texture * textureTwig2 = new E::Texture();
	textureTwig2->LoadWithImagePath("Textures/Tree/Twig/EU55twg2.png");

	E::Texture * textureBark2 = new E::Texture();
	textureBark2->LoadWithImagePath("Textures/Tree/Bark/EU55brk2.png");

	E::Texture * textureTrunk2 = new E::Texture();
	textureTrunk2->LoadWithImagePath("Textures/Tree/Trunk/EU55trk2.png");

	E::Texture * textureTwig1 = new E::Texture();
	textureTwig1->LoadWithImagePath("Textures/Tree/Twig/EU55twg1.png");

	E::Texture * textureRoots2 = new E::Texture();
	textureRoots2->LoadWithImagePath("Textures/Tree/Roots/EU55rts2.png");

	E::Texture * textureBranch3 = new E::Texture();
	textureBranch3->LoadWithImagePath("Textures/Tree/Branch/EU55brn3.png");

	E::Texture * textureBud1 = new E::Texture();
	textureBud1->LoadWithImagePath("Textures/Tree/Bud/EU55bud1.png");

	E::Texture * textureBranch4 = new E::Texture();
	textureBranch4->LoadWithImagePath("Textures/Tree/Branch/EU55brn4.png");

	E::Texture * textureBranch7 = new E::Texture();
	textureBranch7->LoadWithImagePath("Textures/Tree/Branch/EU55brn7.png");
#pragma endregion


	tree2Model = new OBJModel();
	tree2Model->LoadOBJ(std::string("3D models/Tree2WithoutFlowers.obj"));

	treeTwig2 = new Renderer(standardShader, tree2Model->models[0]);
	treeBark2 = new Renderer(standardShader, tree2Model->models[1]);
	treeTrunk2 = new Renderer(standardShader, tree2Model->models[2]);
	treeTwig1 = new Renderer(standardShader, tree2Model->models[3]);
	treeRoots2 = new Renderer(standardShader, tree2Model->models[4]);
	treeBranch3 = new Renderer(standardShader, tree2Model->models[5]);
	treeBud1 = new Renderer(standardShader, tree2Model->models[6]);
	treeBranch4 = new Renderer(standardShader, tree2Model->models[7]);
	treeBranch7 = new Renderer(standardShader, tree2Model->models[8]);

	int multiplier = 50;
	int res = 200;

	trees = new RenderedObject**[numberOfTreeZ];
	for (int z = 0; z < numberOfTreeZ; z++)
	{
		trees[z] = new RenderedObject*[numberOfTreeX];
		for (int x = 0; x < numberOfTreeX; x++)
		{
			float xTranslation = ((rand() / (float)RAND_MAX) /** (rand() % 5)*/);
			float zTranslation = ((rand() / (float)RAND_MAX) /** (rand() % 5)*/);
			float posX = (x + 0.1f + xTranslation) * 15;
			float posZ = (z + 0.1f + zTranslation) * 15;
			trees[z][x] = new RenderedObject
			(
				Transformation
				(
					glm::vec3(posX, ((Utility::Get2DPerlinNoiseValue(posX, posZ, res) + 1) * 0.5 * multiplier) - (multiplier / 2.0f), posZ),
					glm::vec3(),
					glm::vec3(20, 20, 20)
				)
			);

			trees[z][x]->objectParts.push_back(new RenderedObjectPart(textureTwig2, trees[z][x]));
			trees[z][x]->objectParts.push_back(new RenderedObjectPart(textureBark2, trees[z][x]));
			trees[z][x]->objectParts.push_back(new RenderedObjectPart(textureTrunk2, trees[z][x]));
			trees[z][x]->objectParts.push_back(new RenderedObjectPart(textureTwig1, trees[z][x]));
			trees[z][x]->objectParts.push_back(new RenderedObjectPart(textureRoots2, trees[z][x]));
			trees[z][x]->objectParts.push_back(new RenderedObjectPart(textureBranch3, trees[z][x]));
			trees[z][x]->objectParts.push_back(new RenderedObjectPart(textureBud1, trees[z][x]));
			trees[z][x]->objectParts.push_back(new RenderedObjectPart(textureBranch4, trees[z][x]));
			trees[z][x]->objectParts.push_back(new RenderedObjectPart(textureBranch7, trees[z][x]));

			treeTwig2->AddObject(trees[z][x]->objectParts[0]);
			treeBark2->AddObject(trees[z][x]->objectParts[1]);
			treeTrunk2->AddObject(trees[z][x]->objectParts[2]);
			treeTwig1->AddObject(trees[z][x]->objectParts[3]);
			treeRoots2->AddObject(trees[z][x]->objectParts[4]);
			treeBranch3->AddObject(trees[z][x]->objectParts[5]);
			treeBud1->AddObject(trees[z][x]->objectParts[6]);
			treeBranch4->AddObject(trees[z][x]->objectParts[7]);
			treeBranch7->AddObject(trees[z][x]->objectParts[8]);
		}
	}
#pragma endregion


	E::Texture * grassTexture = new E::Texture();
	grassTexture->LoadWithImagePath("Textures/Grass/grass.png");

#pragma region Grass

	grass2Model = new OBJModel();
	grass2Model->LoadOBJ(std::string("3D models/Grass/grass2.obj"));


	grassShader = new StandardShader(ShaderManager::LoadShaderOpenGL("Shaders/Standard/Grass/Vert_Model_Texture_Grass", "Shaders/Standard/Grass/Frag_Model_Texture_Grass"));
	grassShader->SetAmbientLighting(ambientLighting);
	grassShader->SetShineDamper(10.0f);
	grassShader->SetReflectivity(0.0f);
	grassShader->SetLightPosition(light.position);
	grassShader->SetLightColor(light.color);
	grassShader->SetVec3Float(glGetUniformLocation(grassShader->programID, "windDirection"), windDirection);

	grass2Renderer = new Renderer(grassShader, grass2Model->models[0]);


	grass2 = new RenderedObject**[numberOfGrassZ];
	for (int z = 0; z < numberOfGrassZ; z++)
	{
		grass2[z] = new RenderedObject*[numberOfGrassX];
		for (int x = 0; x < numberOfGrassX; x++)
		{
			float posX = (x + 0.1f);
			float posZ = (z + 0.1f);
			grass2[z][x] = new RenderedObject
			(
				Transformation
				(
					glm::vec3(posX, (((Utility::Get2DPerlinNoiseValue(posX, posZ, res) + 1) * 0.5 * multiplier) - (multiplier / 2.0f)), posZ),
					glm::vec3(0, rand() % 360, 0),
					glm::vec3(1, 1, 1)
				));

			grass2[z][x]->objectParts.push_back(new RenderedObjectPart(grassTexture, grass2[z][x]));

			grass2Renderer->AddObject(grass2[z][x]->objectParts[0]);
		}
	}
#pragma endregion




#pragma region Tank
	E::Texture * tankTexture = new E::Texture();
	tankTexture->LoadWithImagePath("Textures/T90-1.jpg");

	tanks = new RenderedObject**[numberOftankZ];
	for (int z = 0; z < numberOftankZ; z++)
	{
		tanks[z] = new RenderedObject*[numberOftankX];
		for (int x = 0; x < numberOftankX; x++)
		{
			tanks[z][x] = new RenderedObject(Transformation(glm::vec3((x + 1) * 5, (((Utility::Get2DPerlinNoiseValue((x + 1) * 5, (z + 1) * 20, res) + 1) * 0.5 * multiplier) - (multiplier / 2.0f)) - 0.1f, (z + 1) * 20), glm::vec3(), glm::vec3(1, 1, 1)));
			tanks[z][x]->objectParts.push_back(new RenderedObjectPart(tankTexture, tanks[z][x]));
		}
	}

	OBJModel * tankModel = new OBJModel();
	tankModel->LoadOBJ(std::string("3D models/T90.obj"));


	StandardShader * tankShader = new StandardShader(ShaderManager::LoadShaderOpenGL("Shaders/Standard/Vert_Model_Texture", "Shaders/Standard/Frag_Model_Texture"));

	tankShader->SetAmbientLighting(ambientLighting);
	tankShader->SetShineDamper(10.0f);
	tankShader->SetReflectivity(1.0f);
	tankShader->SetLightPosition(light.position);
	tankShader->SetLightColor(light.color);


	tankRenderer1 = new Renderer(tankShader, tankModel->models[0]);
	tankRenderer2 = new Renderer(tankShader, tankModel->models[1]);
	tankRenderer3 = new Renderer(tankShader, tankModel->models[2]);
	tankRenderer4 = new Renderer(tankShader, tankModel->models[3]);
	tankRenderer5 = new Renderer(tankShader, tankModel->models[4]);

	for (int z = 0; z < numberOftankZ; z++)
	{
		for (int x = 0; x < numberOftankX; x++)
		{
			tankRenderer1->AddObject(tanks[z][x]->objectParts[0]);
			tankRenderer2->AddObject(tanks[z][x]->objectParts[0]);
			tankRenderer3->AddObject(tanks[z][x]->objectParts[0]);
			tankRenderer4->AddObject(tanks[z][x]->objectParts[0]);
			tankRenderer5->AddObject(tanks[z][x]->objectParts[0]);
		}
	}
#pragma endregion




}

void Scene::Update(float gameTime)
{
	//if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	//{
	//	tank1->transformation.Translate(glm::vec3(-1 * gameTime, 0, 0));
	//}
	//if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	//{
	//	tank1->transformation.Translate(glm::vec3(1 * gameTime, 0, 0));
	//}
	//if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
	//{
	//	tank1->transformation.Rotate(glm::vec3(0, Utility::ConvertirDegreeEnRadian(25) * gameTime, 0));
	//}


	/*tankRenderer->Update();*/

	//windDirection = glm::vec3((rand() / (float)RAND_MAX) / 10.0f, 0, 0);
	//windDirection.x += 0.01f;
	totalTimeForWind += gameTime;
	glm::vec3 windForce(8.0f, 0.0f, 0.0f);
	windDirection.x = 0.5f * sin(0.5f * (totalTimeForWind - 3.2f)) + 0.5f;
	windForce *= windDirection;
	grassShader->SetVec3Float(glGetUniformLocation(grassShader->programID, "windDirection"), windForce);
}


