#include "stdafx.h"
#include "Sphere.h"

Sphere::Sphere()
{
}

Sphere::Sphere(glm::vec3 & pos, glm::vec3 & angle, glm::vec3 & scaling, float rayon, bool createModel) : GameObject(pos, angle, scaling), rayon(rayon)
{
	if (createModel)
	{
		CreateModel();
	}
}


Sphere::~Sphere()
{
}


void Sphere::CreateModel()
{
	float precision = 1;
	int couches = 180 / precision;
	int numberOfPoints = (couches + 1) * (couches * 2);

	std::vector<glm::vec3> points(numberOfPoints);
	CreatePoints(points, couches, precision);

	std::vector<float> vertices(numberOfPoints * 6 * 3);
	CreateVertices(vertices, points, couches);

	std::vector<float> colors(numberOfPoints * 6 * 4);
	CreateColors(colors, numberOfPoints);


	pointCount = numberOfPoints * 6;

	modelMatrixVBO = Model::StaticGenMatVBO(glm::value_ptr(transformation.modelMatrix), 16, 2, 4, GL_STREAM_DRAW, 4, sizeof(float), sizeof(glm::mat4), true);

	allModels = new std::vector<Model*>;
	Model * model = new Model();
	model->GenVAO();
	model->GenVBO(vertices.data(), vertices.size(), GL_STATIC_DRAW, 0, 3, false);
	model->numberOfVertex = vertices.size() / 3;
	model->GenVBO(colors.data(), colors.size(), GL_STATIC_DRAW, 1, 4, false);
	model->AddVBO(modelMatrixVBO);
	model->BindAllVBO();
	allModels->push_back(model);

	modelIsCreated = true;
}

void Sphere::CreatePoints(std::vector<glm::vec3>& points, int couches, float precision)
{
	glm::vec3 baseVecteur = axeY * rayon;
	glm::vec3 pointVecteur = baseVecteur;
	glm::vec3 rotationAxis = axeX;

	glm::vec3 * actualPosition = points.data();
	float * pointVecteurPtr = glm::value_ptr(pointVecteur);
	int vec3Size = 3 * sizeof(float);

	for (int x = 0; x < couches * 2; x++)
	{
		for (int y = 0; y < couches + 1; y++)
		{
			memcpy(actualPosition, pointVecteurPtr, vec3Size);
			pointVecteur = Utility::ClockwiseRotate(precision, pointVecteur, rotationAxis);
			actualPosition += 1;
		}
		rotationAxis = Utility::ClockwiseRotate(precision, rotationAxis, axeY);
		pointVecteur = baseVecteur;
	}
}

void Sphere::CreateVertices(std::vector<float>& vertices, std::vector<glm::vec3>& points, int couches)
{
	int numberOfPoints = points.size();
	float * actualPositionVertices = vertices.data();

	glm::vec3 sG;
	glm::vec3 iG;
	glm::vec3 sD;
	glm::vec3 iD;

	float * sGptr = glm::value_ptr(sG);
	float * iGptr = glm::value_ptr(iG);
	float * sDptr = glm::value_ptr(sD);
	float * iDptr = glm::value_ptr(iD);

	int vec3Size = 3 * sizeof(float);
	for (int i = 0; i < numberOfPoints; i++)
	{
		//s = sup�rieur / i = inf�rieur / d = droite / g = gauche
		sG = points.at(i);
		iG = points.at((i + 1) % numberOfPoints);
		sD = points.at((i + couches) % numberOfPoints);
		iD = points.at(((i + couches) + 1) % numberOfPoints);

		memcpy(actualPositionVertices, sGptr, vec3Size);
		memcpy(actualPositionVertices + 3, iGptr, vec3Size);
		memcpy(actualPositionVertices + 6, iDptr, vec3Size);

		memcpy(actualPositionVertices + 9, iDptr, vec3Size);
		memcpy(actualPositionVertices + 12, sDptr, vec3Size);
		memcpy(actualPositionVertices + 15, sGptr, vec3Size);

		actualPositionVertices += 18;
	}
}

void Sphere::CreateColors(std::vector<float>& colors, int numberOfPoints)
{
	float * actualPositionColors = colors.data();

	glm::vec4 color;
	glm::vec4 color2;

	float * colorPtr = glm::value_ptr(color);
	float * color2Ptr = glm::value_ptr(color2);

	int vec4Size = 4 * sizeof(float);
	for (int i = 0; i < numberOfPoints; i++)
	{
		color = glm::vec4((float)rand() / RAND_MAX, (float)rand() / RAND_MAX, (float)rand() / RAND_MAX, 1);
		color2 = glm::vec4((float)rand() / RAND_MAX, (float)rand() / RAND_MAX, (float)rand() / RAND_MAX, 1);

		memcpy(actualPositionColors, colorPtr, vec4Size);
		memcpy(actualPositionColors + 4, colorPtr, vec4Size);
		memcpy(actualPositionColors + 8, colorPtr, vec4Size);

		memcpy(actualPositionColors + 12, color2Ptr, vec4Size);
		memcpy(actualPositionColors + 16, color2Ptr, vec4Size);
		memcpy(actualPositionColors + 20, color2Ptr, vec4Size);

		actualPositionColors += 24;
	}
}

void Sphere::Draw()
{
	shader->Use();
	if (transformation.modelMatrixUpdated)
	{
		transformation.modelMatrixUpdated = false;
		modelMatrixVBO->SetDataInVBO(glm::value_ptr(transformation.modelMatrix), 16, sizeof(float), GL_STREAM_DRAW);
	}

	glBindVertexArray(allModels->at(0)->vaoID);
	glDrawArraysInstanced(GL_TRIANGLES, 0, pointCount, 1);
}
