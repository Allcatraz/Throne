#include "stdafx.h"
#include "Model.h"


using namespace E;

Model::Model()
{

}

Model::Model(std::vector<float> & vertices, std::vector<float> & texCoords, GLenum drawType) : Model(vertices.data(), texCoords.data(), vertices.size(), texCoords.size(), drawType)
{
}

Model::Model(float * vertices, float * texCoords, int sizeVertices, int sizeCoords, GLenum drawType)
{
	numberOfVertex = sizeVertices / 3;
	GenVBO(vertices, sizeVertices, drawType, 0, 3, false);
	GenVBO(texCoords, sizeCoords, drawType, 1, 2, false);
	GenVAO();

	BindAllVBO();

	glBindVertexArray(0);
}

Model::Model(std::vector<float> & vertices, std::vector<float> & colors, GLenum drawType, bool isColor) : Model(vertices.data(), colors.data(), vertices.size(), colors.size(), drawType, isColor)
{
}


Model::Model(float * vertices, float * colors, int sizeVertices, int sizeColors, GLenum drawType, bool isColor)
{
	numberOfVertex = sizeVertices / 3;
	GenVBO(vertices, sizeVertices, drawType, 0, 3, false);
	GenVBO(colors, sizeColors, drawType, 1, 4, false);
	GenVAO();

	BindAllVBO();

	glBindVertexArray(0);
}


void Model::GenVAO()
{
	glGenVertexArrays(1, &vaoID);
}


#pragma region Member
void Model::AddVBO(E::VBO * vbo)
{
	vbos.push_back(vbo);
}
void Model::GenVBO(int locationInShader, int dataSize, GLenum dataType, bool isInstanced)
{
	AddVBO(StaticGenVBO(locationInShader, dataSize, dataType, isInstanced));
}

void Model::GenVBO(int * data, int count, GLenum drawType, int locationInShader, int dataSize, bool isInstanced)
{
	AddVBO(StaticGenVBO(data, count, drawType, locationInShader, dataSize, isInstanced));
}

void Model::GenVBO(float * data, int count, GLenum drawType, int locationInShader, int dataSize, bool isInstanced)
{
	AddVBO(StaticGenVBO(data, count, drawType, locationInShader, dataSize, isInstanced));
}

void Model::GenVBO(double * data, int count,  GLenum drawType, int locationInShader, int dataSize, bool isInstanced)
{
	AddVBO(StaticGenVBO(data, count, drawType, locationInShader, dataSize, isInstanced));
}

void Model::GenMatVBO(int locationInShader, int dataSize, GLenum dataType, int numberOfLines, int dataTypeSizeInBytes, int matTypeSizeInByte, bool isInstanced)
{
	AddVBO(StaticGenMatVBO(locationInShader, dataSize, dataType, numberOfLines, dataTypeSizeInBytes, matTypeSizeInByte, isInstanced));
}

void Model::GenMatVBO(float * data, int count, int locationInShader, int dataSize, GLenum drawType, int numberOfLines, int dataTypeSizeInBytes, int matTypeSizeInByte, bool isInstanced)
{
	AddVBO(StaticGenMatVBO(data, count,  locationInShader, dataSize, drawType,numberOfLines, dataTypeSizeInBytes, matTypeSizeInByte, isInstanced));
}

void Model::GenMatVBO(double * data, int count, int locationInShader, int dataSize, GLenum drawType, int numberOfLines, int dataTypeSizeInBytes, int matTypeSizeInByte, bool isInstanced)
{
	AddVBO(StaticGenMatVBO(data, count,  locationInShader, dataSize, drawType, numberOfLines, dataTypeSizeInBytes, matTypeSizeInByte, isInstanced));
}


void Model::SetDataInVBO(int vboIndex, int * data, int count, int dataTypeSizeInBytes, GLenum drawType)
{
	vbos.at(vboIndex)->SetDataInVBO(data, count, dataTypeSizeInBytes, drawType);
}

void Model::SetDataInVBO(int vboIndex, float * data, int count, int dataTypeSizeInBytes, GLenum drawType)
{
	vbos.at(vboIndex)->SetDataInVBO(data, count, dataTypeSizeInBytes, drawType);
}

void Model::SetDataInVBO(int vboIndex, double * data, int count, int dataTypeSizeInBytes, GLenum drawType)
{
	vbos.at(vboIndex)->SetDataInVBO(data, count, dataTypeSizeInBytes, drawType);
}

#pragma endregion



#pragma region Static
E::VBO * Model::StaticGenVBO(int locationInShader, int dataSize, GLenum dataType, bool isInstanced)
{
	if (isInstanced)
	{
		return new InstancedVBO(locationInShader, dataSize, dataType);
	}
	else
	{
		return new VBO(locationInShader, dataSize, dataType);
	}
}
E::VBO * Model::StaticGenVBO(int * data, int count, GLenum drawType, int locationInShader, int dataSize, bool isInstanced)
{
	VBO* vbo = StaticGenVBO(locationInShader, dataSize, GL_INT, isInstanced);
	SetDataInVBO(vbo, data, count, sizeof(int), drawType);
	return vbo;
}

E::VBO * Model::StaticGenVBO(float * data, int count, GLenum drawType, int locationInShader, int dataSize, bool isInstanced)
{
	VBO* vbo = StaticGenVBO(locationInShader, dataSize, GL_FLOAT, isInstanced);
	SetDataInVBO(vbo, data, count, sizeof(float), drawType);
	return vbo;
}

E::VBO * Model::StaticGenVBO(double * data, int count, GLenum drawType, int locationInShader, int dataSize, bool isInstanced)
{
	VBO* vbo = StaticGenVBO(locationInShader, dataSize, GL_DOUBLE, isInstanced);
	SetDataInVBO(vbo, data, count, sizeof(double), drawType);
	return vbo;
}

E::VBO * Model::StaticGenMatVBO(int locationInShader, int dataSize, GLenum dataType, int numberOfLines, int dataTypeSizeInBytes, int matTypeSizeInByte, bool isInstanced)
{
	if (isInstanced)
	{
		return new InstancedMatVBO(locationInShader, dataSize, dataType, numberOfLines, dataTypeSizeInBytes, matTypeSizeInByte);
	}
	else
	{
		return new MatVBO(locationInShader, dataSize, dataType, numberOfLines, dataTypeSizeInBytes, matTypeSizeInByte);
	}
}

E::VBO * Model::StaticGenMatVBO(float * data, int count, int locationInShader, int dataSize, GLenum drawType, int numberOfLines, int dataTypeSizeInBytes, int matTypeSizeInByte, bool isInstanced)
{
	VBO* vbo = StaticGenMatVBO(locationInShader, dataSize, GL_FLOAT, numberOfLines, dataTypeSizeInBytes, matTypeSizeInByte, isInstanced);
	SetDataInVBO(vbo, data, count, sizeof(float), drawType);
	return vbo;
}

E::VBO * Model::StaticGenMatVBO(double * data, int count, int locationInShader, int dataSize, GLenum drawType, int numberOfLines, int dataTypeSizeInBytes, int matTypeSizeInByte, bool isInstanced)
{
	VBO* vbo = StaticGenMatVBO(locationInShader, dataSize, GL_DOUBLE, numberOfLines, dataTypeSizeInBytes, matTypeSizeInByte, isInstanced);
	SetDataInVBO(vbo, data, count, sizeof(double), drawType);
	return vbo;
}

void Model::SetDataInVBO(E::VBO * vbo, int * data, int count, int dataTypeSizeInBytes, GLenum drawType)
{
	vbo->SetDataInVBO(data, count, dataTypeSizeInBytes, drawType);
}

void Model::SetDataInVBO(E::VBO * vbo, float * data, int count, int dataTypeSizeInBytes, GLenum drawType)
{
	vbo->SetDataInVBO(data, count, dataTypeSizeInBytes, drawType);
}

void Model::SetDataInVBO(E::VBO * vbo, double * data, int count, int dataTypeSizeInBytes, GLenum drawType)
{
	vbo->SetDataInVBO(data, count, dataTypeSizeInBytes, drawType);
}

#pragma endregion




void Model::BindAllVBO()
{
	glBindVertexArray(vaoID);
	for (int i = 0; i < vbos.size(); i++)
	{	
		vbos[i]->BindToVAO();
	}
}


Model::~Model()
{
	//for (int i = 0; i < vbos.size(); i++)
	//{
	//	if (glIsBuffer(vbos[i])) 
	//	{
	//		glDeleteBuffers(1, &vbos[i]);
	//	}
	//}
	//if (glIsVertexArray(vaoID))
	//{
	//	glDeleteVertexArrays(1, &vaoID);
	//}


}
