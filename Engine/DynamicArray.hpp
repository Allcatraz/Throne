#pragma once
#include "Data.hpp"
#include <vector>
#include <algorithm>

template<typename T>
class DynamicArray
{
public:
	DynamicArray();
	DynamicArray(int _dataSize);
	~DynamicArray();
	int dataSize = 0;
	std::vector<int> allId;
	std::vector<int> erases;
	std::vector<Data<T>> inserts;
	std::vector<int> insertsID;
	int futureSize = 0;
	Data<T> data;
	void Erase(int id);
	void Erase(int * id, int size);
	void Erase(std::vector<int> id);
	void Erase(Data<int> id);

	void Insert(int id, T * data);
	void Insert(int id, std::vector<T> data);
	void Insert(int id, Data<T> data);
	void Insert(std::vector<int> id, std::vector<std::vector<T>> data);
	void Insert(Data<int> id, Data<Data<T>> data);

	void ReserveErases(int size);
	void ReserveInsert(int size);

	int IdSearch(int id);

	void Update();

};

template<typename T>
inline DynamicArray<T>::DynamicArray(int _dataSize) : dataSize(_dataSize)
{
}

template<typename T>
inline void DynamicArray<T>::Erase(int id)
{
	if (data.size > 0)
	{
		erases.push_back(id);
		futureSize -= 3;
	}
}

template<typename T>
inline void DynamicArray<T>::Insert(int id, T * data)
{
	inserts.push_back(Data<T>(3, data));
	insertsID.push_back(id);
	futureSize += 3;
}

template<typename T>
inline int DynamicArray<T>::IdSearch(int id)
{
	int index = allId.size() / 2;
	int actualSize = allId.size();
	bool notFound = true;
	while (notFound)
	{
		if (allId[index] == id)
		{
			return index;
		}
		else if(allId[index] > id)
		{
			index += (actualSize - index);	
		}
		else if(allId[index] < id)
		{
			index /= 2;
		}
		actualSize /= 2;
	}
	return -1;
}

template<typename T>
inline void DynamicArray<T>::Update()
{
	Data<T> dataTemp(futureSize, new T[futureSize]);

}
