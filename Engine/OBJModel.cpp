#include "stdafx.h"
#include "OBJModel.h"
#include <fstream>
#include "TriangleIndices.h"





OBJModel::OBJModel()
{
}


OBJModel::~OBJModel()
{
}

void OBJModel::LoadOBJ(std::string & filePath)
{
	StaticLoadOBJ(filePath, this);
}

bool OBJModel::StaticLoadOBJ(std::string & filePath, OBJModel* objModel)
{
	if (Utility::CheckFilePathExtension(filePath, std::string("obj")))
	{
		std::ifstream file(filePath);

		if (file.is_open() == true)
		{


			int lineNumber = 0;
			int numberOfo = 0;


			bool fileIsOver = false;
			std::vector<std::string> stringVertices;
			std::vector<std::string> stringNormals;
			std::vector<std::string> stringTextureCoords;
			bool firstObject = true;
			while (fileIsOver == false)
			{
				std::vector<TriangleIndices> triangleIndices;

				int numberOfTriangles = 0;

				{
					std::vector<std::string> temp;

					std::vector<std::string> vert1;
					std::vector<std::string> vert2;
					std::vector<std::string> vert3;

					std::string line;
					


					bool objectIsOver = false;
					while (objectIsOver == false)
					{
						fileIsOver = !std::getline(file, line);
						if (fileIsOver == true)
						{
							objectIsOver = true;
						}
						lineNumber++;
						if (STRING_SIZE_IS_HIGHER_DEBUG(line, 1) AND_DEBUG line[0] == 'v')
						{
							IF_STRING_SIZE_IS_HIGHER_DEBUG(line, 2)
								temp = Utility::SplitString(line, ' ');
							if (line[1] == ' ')
							{
								stringVertices.insert(stringVertices.end(), temp.begin() + 1, temp.end());
							}
							else if (line[1] == 'n')
							{
								stringNormals.insert(stringNormals.end(), temp.begin() + 1, temp.end());
							}
							else if (line[1] == 't')
							{
								stringTextureCoords.insert(stringTextureCoords.end(), temp.begin() + 1, temp.end());
							}
							CLOSED_BRACKET_DEBUG
						}
						else if (STRING_SIZE_IS_HIGHER_DEBUG(line, 1) AND_DEBUG line[0] == 'f')
						{
							temp = Utility::SplitString(line, ' ');
							vert1 = Utility::SplitString(temp[1], '/');
							vert2 = Utility::SplitString(temp[2], '/');
							vert3 = Utility::SplitString(temp[3], '/');
							TriangleIndices t(
								std::stoi(vert1[0]) - 1, std::stoi(vert2[0]) - 1, std::stoi(vert3[0]) - 1,
								std::stoi(vert1[2]) - 1, std::stoi(vert2[2]) - 1, std::stoi(vert3[2]) - 1,
								std::stoi(vert1[1]) - 1, std::stoi(vert2[1]) - 1, std::stoi(vert3[1]) - 1);
							triangleIndices.push_back(t);
							numberOfTriangles++;
						}
						else if (STRING_SIZE_IS_HIGHER_DEBUG(line, 2) AND_DEBUG line[0] == 'o' && line[1] == ' ')
						{
							numberOfo++;
							if (firstObject == false)
							{
								objectIsOver = true;
							}
							else
							{
								firstObject = false;
							}			
						}
					}
				}


				float * vertices = new float[numberOfTriangles * 3 * 3];
				float * normals = new float[numberOfTriangles * 3 * 3];
				float * textureIndex = new float[numberOfTriangles * 3 * 2];

				int index = 0;
				int indexTexture = 0;
				for (TriangleIndices & t : triangleIndices)
				{

					int index1 = t.index1 * 3;
					vertices[index + 0] = std::stof(stringVertices[index1 + 0]);
					vertices[index + 1] = std::stof(stringVertices[index1 + 1]);
					vertices[index + 2] = std::stof(stringVertices[index1 + 2]);

					int index2 = t.index2 * 3;
					vertices[index + 3] = std::stof(stringVertices[index2 + 0]);
					vertices[index + 4] = std::stof(stringVertices[index2 + 1]);
					vertices[index + 5] = std::stof(stringVertices[index2 + 2]);

					int index3 = t.index3 * 3;
					vertices[index + 6] = std::stof(stringVertices[index3 + 0]);
					vertices[index + 7] = std::stof(stringVertices[index3 + 1]);
					vertices[index + 8] = std::stof(stringVertices[index3 + 2]);




					glm::vec3 normal1(std::stof(stringNormals[t.normalIndex1 * 3]), std::stof(stringNormals[t.normalIndex1 * 3 + 1]), std::stof(stringNormals[t.normalIndex1 * 3 + 2]));
					glm::vec3 normal2(std::stof(stringNormals[t.normalIndex2 * 3]), std::stof(stringNormals[t.normalIndex2 * 3 + 1]), std::stof(stringNormals[t.normalIndex2 * 3 + 2]));
					glm::vec3 normal3(std::stof(stringNormals[t.normalIndex3 * 3]), std::stof(stringNormals[t.normalIndex3 * 3 + 1]), std::stof(stringNormals[t.normalIndex3 * 3 + 2]));

					normals[index] = normal1.x;
					normals[index + 1] = normal1.y;
					normals[index + 2] = normal1.z;

					normals[index + 3] = normal2.x;
					normals[index + 4] = normal2.y;
					normals[index + 5] = normal2.z;

					normals[index + 6] = normal3.x;
					normals[index + 7] = normal3.y;
					normals[index + 8] = normal3.z;


					index += 9;



					int textIndex1 = t.textIndex1 * 2;
					textureIndex[indexTexture + 0] = std::stof(stringTextureCoords[textIndex1 + 0]);
					textureIndex[indexTexture + 1] = std::stof(stringTextureCoords[textIndex1 + 1]);

					int textIndex2 = t.textIndex2 * 2;
					textureIndex[indexTexture + 2] = std::stof(stringTextureCoords[textIndex2 + 0]);
					textureIndex[indexTexture + 3] = std::stof(stringTextureCoords[textIndex2 + 1]);

					int textIndex3 = t.textIndex3 * 2;
					textureIndex[indexTexture + 4] = std::stof(stringTextureCoords[textIndex3 + 0]);
					textureIndex[indexTexture + 5] = std::stof(stringTextureCoords[textIndex3 + 1]);


					indexTexture += 6;
				}

				Model * model = new Model();
				model->GenVAO();
				model->GenVBO(vertices, numberOfTriangles * 3 * 3, GL_STATIC_DRAW, 0, 3, false);
				model->numberOfVertex = numberOfTriangles * 3;
				model->GenVBO(normals, numberOfTriangles * 3 * 3, GL_STATIC_DRAW, 1, 3, false);
				model->GenVBO(textureIndex, numberOfTriangles * 3 * 2, GL_STATIC_DRAW, 2, 2, false);
				model->BindAllVBO();


				objModel->models.push_back(model);
				int a = 0;
			}
			file.close();
			return true;
		}
		else
		{
			std::cout << "Could not open file : " << filePath << "\n";
		}
	}
	else
	{
		std::cout << "File : \" " << filePath << " \" is not an .obj" << "\n";
	}

	return false;
}



