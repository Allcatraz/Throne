#include "stdafx.h"
#include "TriangleIndices.h"


TriangleIndices::TriangleIndices()
{
}

TriangleIndices::TriangleIndices(
	int index1, int index2, int index3, 
	int normalIndex1, int normalIndex2, int normalIndex3, 
	int textIndex1, int textIndex2, int textIndex3) :
	index1(index1), index2(index2), index3(index3), 
	normalIndex1(normalIndex1), normalIndex2(normalIndex2), normalIndex3(normalIndex3), 
	textIndex1(textIndex1), textIndex2(textIndex2), textIndex3(textIndex3)
{
}


TriangleIndices::~TriangleIndices()
{
}
