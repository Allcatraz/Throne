#pragma once
#include "Transformation.h"
#include "RenderedObjectPart.h"
#include "Texture.h"


class RenderedObject
{
public:
	RenderedObject();
	RenderedObject(Transformation& transformation);
	~RenderedObject();

	std::vector<RenderedObjectPart*> objectParts;
	Transformation transformation;
};

