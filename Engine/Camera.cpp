#include "stdafx.h"
#include "Camera.h"

Camera::Camera() : phi(0), theta(0), orientation(), verticalAxe(0, 0, 1), lateralMove(), position(), viewPoint()
{
}

Camera::Camera(vec3& position, vec3& viewPoint, vec3& verticalAxe) : 
	phi(0), theta(0), orientation(), verticalAxe(verticalAxe), lateralMove(), position(position), viewPoint(viewPoint)
{
	setViewPoint(viewPoint);
}

Camera::~Camera()
{
}

void Camera::moveOrientation(int xRel, int yRel)
{
	phi += -yRel * 0.05f;
	theta += -xRel * 0.05f;
	if (phi > 89)
		phi = 89;
	else if (phi < -89)
		phi = -89;
	float radPhi = Utility::ConvertirDegreeEnRadian(phi);
	float radTheta = Utility::ConvertirDegreeEnRadian(theta);
	if (verticalAxe.x == 1.0f)
	{
		orientation.x = sin(radPhi);
		orientation.y = cos(radPhi) * cos(radTheta);
		orientation.z = cos(radPhi) * sin(radTheta);
	}
	else if (verticalAxe.y == 1.0f)
	{
		orientation.x = cos(radPhi) * sin(radTheta);
		orientation.y = sin(radPhi);
		orientation.z = cos(radPhi) * cos(radTheta);
	}
	else
	{
		orientation.x = cos(radPhi) * cos(radTheta);
		orientation.y = cos(radPhi) * sin(radTheta);
		orientation.z = sin(radPhi);
	}
	lateralMove = cross(verticalAxe, orientation);
	lateralMove = normalize(lateralMove);
	viewPoint = position + orientation;
}

vec3 Camera::GetPosition()
{
	return position;
}

vec3 Camera::GetViewPoint()
{
	return viewPoint;
}

void Camera::move(float timeInSeconds)
{
	if (Keyboard::isKeyPressed(Keyboard::W))
	{
		position = position + orientation * speed * timeInSeconds;
		viewPoint = position + orientation;
	}
	if (Keyboard::isKeyPressed(Keyboard::S))
	{
		position = position - orientation * speed * timeInSeconds;
		viewPoint = position + orientation;
	}
	if (Keyboard::isKeyPressed(Keyboard::Q))
	{
		position = position + lateralMove * speed * timeInSeconds;
		viewPoint = position + orientation;
	}
	if (Keyboard::isKeyPressed(Keyboard::E))
	{
		position = position - lateralMove * speed * timeInSeconds;
		viewPoint = position + orientation;
	}
}

void Camera::lookAt(mat4& modelview)
{
	modelview = glm::lookAt(position, viewPoint, verticalAxe);
}

void Camera::setViewPoint(vec3 viewPoint)
{
	orientation = viewPoint - position;
	orientation = normalize(orientation);
	if (verticalAxe.x == 1)
	{
		phi = asin(orientation.x);
		theta = acos(orientation.y / cos(phi));
		if (orientation.y < 0)
			theta *= -1;
	}
	else if (verticalAxe.y == 1)
	{
		phi = asin(orientation.y);
		theta = acos(orientation.z / cos(phi));
		if (orientation.z < 0)
			theta *= -1;
	}
	else
	{
		phi = asin(orientation.x);
		theta = acos(orientation.z / cos(phi));
		if (orientation.z < 0)
			theta *= -1;
	}
	phi = Utility::ConvertirRadianEnDegre(phi);
	theta = Utility::ConvertirRadianEnDegre(theta);
}
