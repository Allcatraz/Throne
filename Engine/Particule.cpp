#include "stdafx.h"
#include "Particule.h"


Particule::Particule()
{
}


Particule::Particule(glm::vec3 _position, glm::vec3 _velocity, float _gravityEffect, float _rotation, float _scale, float _lifeLength) : 
	position(_position), velocity(_velocity), gravityEffect(_gravityEffect), rotation(_rotation), scale(_scale), lifeLength(_lifeLength)
{
}


Particule::~Particule()
{
}

