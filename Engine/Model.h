#pragma once
#include "VBO.h"
#include "MatVBO.h"
#include "InstancedVBO.h"
#include "InstancedMatVBO.h"

class Model
{
public:
	Model();
	Model(std::vector<float> & vertices, std::vector<float> & texCoords, GLenum drawType);
	Model(float * vertices, float * texCoords, int sizeVertices, int sizeCoords, GLenum drawType);

	Model(std::vector<float> & vertices, std::vector<float> & colors, GLenum drawType, bool isColor);
	Model(float * vertices, float * colors, int sizeVertices, int sizeColors, GLenum drawType, bool isColor);



	void GenVAO();


	void GenVBO(int locationInShader, int dataSize, GLenum dataType, bool isInstanced);
	void GenVBO(int * data, int count, GLenum drawType, int locationInShader, int dataSize, bool isInstanced);
	void GenVBO(float * data, int count, GLenum drawType, int locationInShader, int dataSize, bool isInstanced);
	void GenVBO(double * data, int count, GLenum drawType, int locationInShader, int dataSize, bool isInstanced);

	void GenMatVBO(int locationInShader, int dataSize, GLenum dataType, int numberOfLines, int dataTypeSizeInBytes, int matTypeSizeInByte, bool isInstanced);
	void GenMatVBO(float * data, int count, int locationInShader, int dataSize, GLenum drawType, int numberOfLines, int dataTypeSizeInBytes, int matTypeSizeInByte, bool isInstanced);
	void GenMatVBO(double * data, int count, int locationInShader, int dataSize, GLenum drawType, int numberOfLines, int dataTypeSizeInBytes, int matTypeSizeInByte, bool isInstanced);

	void SetDataInVBO(int vboIndex, int * data, int count, int dataTypeSizeInBytes, GLenum drawType);
	void SetDataInVBO(int vboIndex, float * data, int count, int dataTypeSizeInBytes, GLenum drawType);
	void SetDataInVBO(int vboIndex, double * data, int count, int dataTypeSizeInBytes, GLenum drawType);

	void AddVBO(E::VBO* vbo);


	static E::VBO* StaticGenVBO(int locationInShader, int dataSize, GLenum dataType, bool isInstanced);
	static E::VBO* StaticGenVBO(int * data, int count, GLenum drawType, int locationInShader, int dataSize, bool isInstanced);
	static E::VBO* StaticGenVBO(float * data, int count, GLenum drawType, int locationInShader, int dataSize, bool isInstanced);
	static E::VBO* StaticGenVBO(double * data, int count, GLenum drawType, int locationInShader, int dataSize, bool isInstanced);

	static E::VBO* StaticGenMatVBO(int locationInShader, int dataSize, GLenum dataType, int numberOfLines, int dataTypeSizeInBytes, int matTypeSizeInByte, bool isInstanced);
	static E::VBO* StaticGenMatVBO(float * data, int count, int locationInShader, int dataSize, GLenum drawType, int numberOfLines, int dataTypeSizeInBytes, int matTypeSizeInByte, bool isInstanced);
	static E::VBO* StaticGenMatVBO(double * data, int count, int locationInShader, int dataSize, GLenum drawType, int numberOfLines, int dataTypeSizeInBytes, int matTypeSizeInByte, bool isInstanced);

	static void SetDataInVBO(E::VBO* vbo, int * data, int count, int dataTypeSizeInBytes, GLenum drawType);
	static void SetDataInVBO(E::VBO* vbo, float * data, int count, int dataTypeSizeInBytes, GLenum drawType);
	static void SetDataInVBO(E::VBO* vbo, double * data, int count, int dataTypeSizeInBytes, GLenum drawType);


	void BindAllVBO();


	~Model();

	GLuint vaoID;
	int numberOfVertex = 0;
private:
	std::vector<E::VBO*> vbos;

};
