#pragma once
#include "VBO.h"
namespace E
{
	class MatVBO : public VBO
	{
	public:
		MatVBO();
		MatVBO(GLuint _location, int _dataSize, GLenum _dataType, int _numberOfLines, int _dataTypeSizeInBytes, int _matTypeSizeInByte);
		virtual void BindToVAO() override;
		
		~MatVBO();
	protected:
		int numberOfLines;
		int dataTypeSizeInBytes;
		int matTypeSizeInByte;
		void AttribPointer(int i);
	};
}


