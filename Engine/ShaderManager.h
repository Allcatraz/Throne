#pragma once
#include "stdafx.h"

static class ShaderManager
{
public:
	ShaderManager();
	~ShaderManager();

	static std::vector<E::Shader *> allShaders;

	static void LoadShaderOpenGL(E::Shader * shader, const std::string & vertPath, const std::string & fragPath);
	static E::Shader * LoadShaderOpenGL(const std::string & vertPath, const std::string & fragPath);

	static void SetProjectionMatrix(float * ptr);
	static void SetViewMatrix(float * ptr);

};

