// stdafx.h�: fichier Include pour les fichiers Include syst�me standard,
// ou les fichiers Include sp�cifiques aux projets qui sont utilis�s fr�quemment,
// et sont rarement modifi�s
//

#pragma once

#define _SCL_SECURE_NO_WARNINGS
#pragma warning(disable:4996)  

#ifndef NDEBUG
#define IF_STRING_SIZE_IS_HIGHER_DEBUG(str, max) if(STRING_SIZE_IS_HIGHER_DEBUG(str, max)) OPEN_BRACKET_DEBUG
#define STRING_SIZE_IS_HIGHER_DEBUG(str, max) str.size() >= max
#define AND_DEBUG &&
#define OPEN_BRACKET_DEBUG {
#define CLOSED_BRACKET_DEBUG }

#endif // DEBUG


#ifdef NDEBUG
#define IF_STRING_SIZE_IS_HIGHER_DEBUG(str, max)
#define STRING_SIZE_IS_HIGHER_DEBUG(str, max)
#define AND_DEBUG
#define OPEN_BRACKET_DEBUG
#define CLOSED_BRACKET_DEBUG

#endif // DEBUG


#include "targetver.h"

#include <stdio.h>
#include <tchar.h>


#include "Constantes.h"
#include "Utility.h"
#include <SFML/Graphics.hpp>
#include <string>
#include "Shader.h"
#include "glm\glm.hpp"
#include "glm\gtx\transform.hpp"
#include "glm\gtc\type_ptr.hpp"
#include <GL\glew.h>



// TODO: faites r�f�rence ici aux en-t�tes suppl�mentaires n�cessaires au programme
