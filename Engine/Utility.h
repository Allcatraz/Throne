#pragma once
#include "stdafx.h"
#include <SFML/Graphics.hpp>
#include <GL\glew.h>
#include "glm\glm.hpp"
#include "glm\gtx\transform.hpp"
#include "glm\gtc\type_ptr.hpp"

class Utility
{
public:
	static float DistanceEntreDeuxPoints(sf::Vector2f position1, sf::Vector2f position2);
	static float CalculCathetesAvecHypothenuse(float hypothenuse);
	static float ConvertirRadianEnDegre(float angleRadian);
	static float ConvertirDegreeEnRadian(float angleDegre);

	static sf::Vector2f CalculerVelocity(sf::Vector2f position, sf::Vector2f destination);
	static float Get2DPerlinNoiseValue(float x, float y, float res);
	static float AngleEntre2Vecteurs(glm::vec3 vec1, glm::vec3 vec2);
	static float ProduitScalaire(glm::vec3 vec1, glm::vec3 vec2);
	static float CalculerNorme(glm::vec3 vec);

	static glm::vec3 ClockwiseRotate(float angle, glm::vec3 vecteur, glm::vec3 axe);
	static glm::mat3 ConstructionMatriceDeRotation3x3Clockwise(float angle, glm::vec3 axe);
	static glm::mat4 ConstructionMatriceDeRotation4x4Clockwise(float angle, glm::vec3 axe);

	static bool CheckFilePathExtension(std::string& filePath, std::string& extension);
	static std::vector<std::string> SplitString(std::string& str, char token);

#define PI 3.14159265358979323846
};

