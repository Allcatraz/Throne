#include "stdafx.h"
#include "MatVBO.h"


E::MatVBO::MatVBO()
{

}

E::MatVBO::MatVBO(GLuint _location, int _dataSize, GLenum _dataType, int _numberOfLines, int _dataTypeSizeInBytes, int _matTypeSizeInByte) : VBO(_location, _dataSize, _dataType),
	numberOfLines(_numberOfLines), dataTypeSizeInBytes(_dataTypeSizeInBytes), matTypeSizeInByte(_matTypeSizeInByte)
{

}

void E::MatVBO::BindToVAO()
{
	glBindBuffer(GL_ARRAY_BUFFER, id);
	for (int i = 0; i < numberOfLines; i++)
	{
		AttribPointer(i);
	}
}

void E::MatVBO::AttribPointer(int i)
{
	switch (dataType)
	{
	case GL_FLOAT:
		glVertexAttribPointer(location + i, dataSize, GL_FLOAT, GL_FALSE, matTypeSizeInByte, (const GLvoid*)(dataTypeSizeInBytes * i * dataSize));
		break;
	case GL_DOUBLE:
		glVertexAttribLPointer(location + i, dataSize, GL_DOUBLE, matTypeSizeInByte, (const GLvoid*)(dataTypeSizeInBytes * i * dataSize));
		break;
	default:
		break;
	}
	glEnableVertexAttribArray(location + i);
}


E::MatVBO::~MatVBO()
{

}
