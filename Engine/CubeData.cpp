#include "stdafx.h"
#include "CubeData.h"



#pragma region STATIC
static float staticVertices[CubeData::numberOfVerticesInCube]
{
	//front face
	0.0f, 0.0f, 0.0f	,	0.0f, -1.0f, 0.0f	,	1.0f, -1.0f, 0.0f	,
	0.0f, 0.0f, 0.0f	,	1.0f,  0.0f, 0.0f	,	1.0f, -1.0f, 0.0f	,

	//right face
	1.0f, 0.0f, 0.0f	,	1.0f, -1.0f, 0.0f	,	1.0f, -1.0f, 1.0f	,
	1.0f, 0.0f, 0.0f	,	1.0f,  0.0f, 1.0f	,	1.0f, -1.0f, 1.0f	,

	//back face
	1.0f, 0.0f, 1.0f	,	1.0f, -1.0f, 1.0f	,	0.0f, -1.0f, 1.0f	,
	1.0f, 0.0f, 1.0f	,	0.0f,  0.0f, 1.0f	,	0.0f, -1.0f, 1.0f	,

	//left face
	0.0f, 0.0f, 1.0f	,	0.0f, -1.0f, 1.0f	,	0.0f, -1.0f, 0.0f	,
	0.0f, 0.0f, 1.0f	,	0.0f,  0.0f, 0.0f	,	0.0f, -1.0f, 0.0f	,

	//up face
	0.0f, 0.0f, 1.0f	,	0.0f, 0.0f, 0.0f	,	1.0f, 0.0f, 0.0f	,
	0.0f, 0.0f, 1.0f	,	1.0f, 0.0f, 1.0f	,	1.0f, 0.0f, 0.0f	,

	//down face
	0.0f, -1.0f, 1.0f	,	0.0f, -1.0f, 0.0f	,	1.0f, -1.0f, 0.0f	,
	0.0f, -1.0f, 1.0f	,	1.0f, -1.0f, 1.0f	,	1.0f, -1.0f, 0.0f
};
static float staticTexCoords[72]
{
	//front face
	0.0f, 0.0f	,	0.0f, 1.0f	,	1.0f, 1.0f	,
	1.0f, 1.0f	,	1.0f, 0.0f	,	0.0f, 0.0f	,

	//right face
	0.0f, 0.0f	,	0.0f, 1.0f	,	1.0f, 1.0f	,
	1.0f, 1.0f	,	1.0f, 0.0f	,	0.0f, 0.0f	,

	//back face
	0.0f, 0.0f	,	0.0f, 1.0f	,	1.0f, 1.0f	,
	1.0f, 1.0f	,	1.0f, 0.0f	,	0.0f, 0.0f	,

	//left face
	0.0f, 0.0f	,	0.0f, 1.0f	,	1.0f, 1.0f	,
	1.0f, 1.0f	,	1.0f, 0.0f	,	0.0f, 0.0f	,

	//up face
	0.0f, 0.0f	,	0.0f, 1.0f	,	1.0f, 1.0f	,
	1.0f, 1.0f	,	1.0f, 0.0f	,	0.0f, 0.0f	,

	//down face
	0.0f, 0.0f	,	0.0f, 1.0f	,	1.0f, 1.0f	,
	1.0f, 1.0f	,	1.0f, 0.0f	,	0.0f, 0.0f
};


std::vector<float> CubeData::GetStaticVertices()
{
	std::vector<float> vertices;
	vertices.insert(vertices.end(), std::begin(staticVertices), std::end(staticVertices));
	return vertices;
}

std::vector<float> CubeData::GetStaticVertices(Faces face)
{
	std::vector<float> vertices(numberOfVerticesPerCubeFace);
	int begin = (int)face * numberOfVerticesPerCubeFace;
	int end = begin + numberOfVerticesPerCubeFace;
	for (int i = begin, z = 0; i < end; i++, z++)
	{
		vertices[z] = staticVertices[i];
	}
	return vertices;
}

std::vector<float> CubeData::GetStaticVertices(glm::vec3 position)
{
	std::vector<float> vertices(numberOfVerticesInCube);
	for (int i = 0; i < numberOfVerticesInCube; i += 3)
	{
		vertices[i + 0] = staticVertices[i + 0] + position.x;
		vertices[i + 1] = staticVertices[i + 1] + position.y;
		vertices[i + 2] = staticVertices[i + 2] + position.z;
	}
	return vertices;
}

float * CubeData::GetStaticVerticesInArray()
{
	return staticVertices;
}

float * CubeData::GetStaticVerticesInArray(Faces face)
{
	float * vertices = new float[numberOfVerticesPerCubeFace];
	int begin = (int)face * numberOfVerticesPerCubeFace;
	int end = begin + numberOfVerticesPerCubeFace;
	for (int i = begin, z = 0; i < end; i++, z++)
	{
		vertices[z] = staticVertices[i];
	}
	return vertices;
}

float * CubeData::GetStaticVerticesInArray(Faces face, int triangleIndex)
{
	float * vertices = new float[numberOfVerticesPerCubeFace / 2];
	int begin = (int)face * numberOfVerticesPerCubeFace + (triangleIndex * (numberOfVerticesPerCubeFace / 2));
	int end = begin + (numberOfVerticesPerCubeFace / 2);
	for (int i = begin, z = 0; i < end; i++, z++)
	{
		vertices[z] = staticVertices[i];
	}
	return vertices;
}

float * CubeData::GetStaticVerticesInArray(glm::vec3 position)
{
	float * vertices = new float[numberOfVerticesInCube];
	for (int i = 0; i < numberOfVerticesInCube; i++)
	{
		vertices[i + 0] = staticVertices[i + 0] + position.x;
		vertices[i + 1] = staticVertices[i + 1] + position.y;
		vertices[i + 2] = staticVertices[i + 2] + position.z;
	}
	return vertices;
}



std::vector<float> CubeData::GetStaticTexCoords()
{
	std::vector<float> texCoords(numberOfTexCoordsInCube);
	for (int i = 0; i < numberOfTexCoordsInCube; i++)
	{
		texCoords[i] = staticTexCoords[i];
	}
	return texCoords;
}

float * CubeData::GetStaticTexCoords1Face()
{
	float * texCoords = new float[12];
	for (int i = 0; i < 12; i++)
	{
		texCoords[i] = staticTexCoords[i];
	}
	return texCoords;
}

float * CubeData::GetStaticTexCoords1Face(int triangle)
{
	float * texCoords = new float[numberOfTexCoordsPerCubeFace / 2];
	int begin = triangle * (numberOfTexCoordsPerCubeFace / 2);
	int end = begin + (numberOfTexCoordsPerCubeFace / 2);
	for (int i = begin, z = 0; i < end; i++, z++)
	{
		texCoords[z] = staticTexCoords[i];
	}
	return texCoords;
}
#pragma endregion



static int newId = 0;

CubeData::CubeData()
{
	id = newId;
	newId++;
}


