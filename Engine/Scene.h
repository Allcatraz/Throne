#pragma once
#include "Cube.h"
#include "Shader.h"
#include "Sphere.h"
#include "Plane.h"
#include "OBJModel.h"
#include "Light.h"
#include "Renderer.h"
#include "RenderedObject.h"
#include "StandardShader.h"
#include "World.h"


class Scene
{
public:
	Scene();
	~Scene();

	StandardShader * standardShader;


	Cube * cube;
	Sphere * sphere;
	World * world;



	RenderedObject *** tanks;
	int numberOftankX = 1;
	int numberOftankZ = 1;

	Renderer * tankRenderer1;
	Renderer * tankRenderer2;
	Renderer * tankRenderer3;
	Renderer * tankRenderer4;
	Renderer * tankRenderer5;


	int numberOfTreeX = 1;
	int numberOfTreeZ = 1;
	RenderedObject *** trees;

	OBJModel * tree2Model;

	Renderer * treeTwig2;
	Renderer * treeBark2;
	Renderer * treeTrunk2;
	Renderer * treeTwig1;
	Renderer * treeRoots2;
	Renderer * treeBranch3;
	Renderer * treeBud1;
	Renderer * treeBranch4;
	Renderer * treeBranch7;

	int numberOfGrassX = 40;
	int numberOfGrassZ = 40;

	RenderedObject *** grass2;
	
	OBJModel * grass2Model;

	Renderer * grass2Renderer;

	Light light;

	float ambientLighting = 0.0f;
	

	glm::vec3 windDirection = glm::vec3(1, 0, 0);
	float totalTimeForWind = 0;
	StandardShader * grassShader;


	void Draw();
	void Creation();
	void Update(float gameTime);





	float totalTimeInDraw = 0;
	float numberOfDrawCall = 0;
	
	void AfficherTempsMoyenParDrawCall()
	{
		if (numberOfDrawCall > 0)
		{
			std::cout << "Temps total dans draw : " << totalTimeInDraw << "\nTemps moyen par draw : " << totalTimeInDraw / numberOfDrawCall << "\n";
		}
	}





};

