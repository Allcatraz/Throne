#include "stdafx.h"
#include "Shader.h"


E::Shader::Shader() : programID(0)
{
	
}

E::Shader::Shader(const std::string& vertexShaderFile, const std::string& fragmentShaderFile) : programID(0)
{
	std::cout << "beforeLoadShaderInShaderCtor : " << glGetError() << std::endl;
	programID = LoadShader(vertexShaderFile, fragmentShaderFile);
	glUseProgram(programID);
	viewMatrixLocation = glGetUniformLocation(programID, "view");
	projectionMatrixLocation = glGetUniformLocation(programID, "projection");
}

E::Shader::~Shader()
{
	glDeleteProgram(programID);
}

void E::Shader::Use()
{
	glUseProgram(programID);
}

GLuint E::Shader::CompileShader(const GLchar* source, GLenum type) const
{
	GLuint id = glCreateShader(type);

	glShaderSource(id, 1, &source, nullptr);
	glCompileShader(id);

	GLint isSuccess;
	GLchar infoLog[512];

	glGetShaderiv(id, GL_COMPILE_STATUS, &isSuccess);

	if (isSuccess == GL_FALSE)
	{
		glGetShaderInfoLog(id, 512, nullptr, infoLog);
		throw std::runtime_error("Error compiling shader: " + std::string(infoLog));
	}

	return id;
}

void E::Shader::SetMatrix4x4f(GLuint location, float * ptr)
{
	glUseProgram(programID);
	glUniformMatrix4fv(location, 1, GL_FALSE, ptr);
}

void E::Shader::SetMatrix4x4f(GLuint location, glm::mat4 & mat)
{
	SetMatrix4x4f(location, glm::value_ptr(mat));
}

void E::Shader::SetFloat(GLuint location, float value)
{
	glUniform1f(location, value);
}

void E::Shader::SetInt(GLuint location, int value)
{
	glUniform1i(location, value);
}

void E::Shader::SetDouble(GLuint location, double value)
{
	glUniform1d(location, value);
}

void E::Shader::SetVec2Float(GLuint location, float * ptr)
{
	glUniform2fv(location, 1, ptr);
}

void E::Shader::SetVec2Float(GLuint location, glm::vec2& vec)
{
	SetVec2Float(location, glm::value_ptr(vec));
}

void E::Shader::SetVec2Int(GLuint location, int * ptr)
{
	glUniform2iv(location, 1, ptr);
}

void E::Shader::SetVec2Int(GLuint location, glm::tvec2<int>& vec)
{
	SetVec2Int(location, glm::value_ptr(vec));
}

void E::Shader::SetVec2Double(GLuint location, double * ptr)
{
	glUniform2dv(location, 1, ptr);
}

void E::Shader::SetVec2Double(GLuint location, glm::tvec2<double>& vec)
{
	SetVec2Double(location, glm::value_ptr(vec));
}

void E::Shader::SetVec3Float(GLuint location, float * ptr)
{
	glUniform3fv(location, 1, ptr);
}

void E::Shader::SetVec3Float(GLuint location, glm::vec3& vec)
{
	SetVec3Float(location, glm::value_ptr(vec));
}

void E::Shader::SetVec3Int(GLuint location, int * ptr)
{
	glUniform3iv(location, 1, ptr);
}

void E::Shader::SetVec3Int(GLuint location, glm::tvec3<int>& vec)
{
	SetVec3Int(location, glm::value_ptr(vec));
}

void E::Shader::SetVec3Double(GLuint location, double * ptr)
{
	glUniform3dv(location, 1, ptr);
}

void E::Shader::SetVec3Double(GLuint location, glm::tvec3<double>& vec)
{
	SetVec3Double(location, glm::value_ptr(vec));
}

void E::Shader::SetVec4Float(GLuint location, float * ptr)
{
	glUniform4fv(location, 1, ptr);
}

void E::Shader::SetVec4Float(GLuint location, glm::vec4& vec)
{
	SetVec4Float(location, glm::value_ptr(vec));
}

void E::Shader::SetVec4Int(GLuint location, int * ptr)
{
	glUniform4iv(location, 1, ptr);
}

void E::Shader::SetVec4Int(GLuint location, glm::tvec4<int>& vec)
{
	SetVec4Int(location, glm::value_ptr(vec));
}

void E::Shader::SetVec4Double(GLuint location, double * ptr)
{
	glUniform4dv(location, 1, ptr);
}

void E::Shader::SetVec4Double(GLuint location, glm::tvec4<double>& vec)
{
	SetVec4Double(location, glm::value_ptr(vec));
}

void E::Shader::SetViewMatrix(float * ptr)
{
	SetMatrix4x4f(viewMatrixLocation, ptr);
}

void E::Shader::SetViewMatrix(glm::mat4 & mat)
{
	SetViewMatrix(glm::value_ptr(mat));
}

void E::Shader::SetProjectionMatrix(float * ptr)
{
	SetMatrix4x4f(projectionMatrixLocation, ptr);
}

void E::Shader::SetProjectionMatrix(glm::mat4 & mat)
{
	SetProjectionMatrix(glm::value_ptr(mat));
}

void E::Shader::SetMatrix3x3f(GLuint location, float * ptr)
{
	glUniformMatrix3fv(location, 1, GL_FALSE, ptr);
}

void E::Shader::SetMatrix3x3f(GLuint location, glm::mat3 & mat)
{
	SetMatrix3x3f(location, glm::value_ptr(mat));
}

std::string E::Shader::GetSource(const std::string& sourceFile)
{
	std::ifstream inFile(sourceFile + ".glsl");
	std::string source;
	std::stringstream stringStream;

	if (inFile.is_open() == false)
	{
		throw std::runtime_error("Could not open file: " + sourceFile);
	}

	stringStream << inFile.rdbuf();
	source = stringStream.str();

	return source;
}

GLuint E::Shader::CreateProgram(GLuint vertexShaderID, GLuint fragmentShaderID)
{
	GLuint id = glCreateProgram();
	std::cout << "inShaderglCreateProgram : " << glGetError() << std::endl;
	if (glIsProgram(id) == false)
	{
		std::cout << "Not a program : " << glGetError() << std::endl;
	}

	glAttachShader(id, vertexShaderID);
	glAttachShader(id, fragmentShaderID);
	std::cout << "inShaderAttachShader : " << glGetError() << std::endl;

	glLinkProgram(id);
	std::cout << "inShaderLinkProgram : " << glGetError() << std::endl;


	// V�rification du linkage

	GLint linkWorked(0);
	glGetProgramiv(id, GL_LINK_STATUS, &linkWorked);
	std::cout << "inShaderGetProgramiv1 : " << glGetError() << std::endl;

	// S'il y a eu une erreur

	if (linkWorked == GL_FALSE)
	{
		// R�cup�ration de la taille de l'erreur

		GLint tailleErreur(0);
		glGetProgramiv(id, GL_INFO_LOG_LENGTH, &tailleErreur);
		std::cout << "inShaderGetProgramiv2 : " << glGetError() << std::endl;


		// Allocation de m�moire

		char *erreur = new char[tailleErreur + 1];


		// R�cup�ration de l'erreur

		glGetShaderInfoLog(id, tailleErreur, &tailleErreur, erreur);
		erreur[tailleErreur] = '\0';
		std::cout << "inShaderGetShaderInfoLog : " << glGetError() << std::endl;


		// Affichage de l'erreur

		std::cout << erreur << std::endl;


		// Lib�ration de la m�moire et retour du bool�en false

		delete[] erreur;
		glDeleteProgram(id);
		std::cout << "inShaderDeleteProgram : " << glGetError() << std::endl;
	}
	return id;
}

GLuint E::Shader::LoadShader(const std::string& vertexShaderFile, const std::string& fragmentShaderFile)
{

	std::string vertexSource = GetSource(vertexShaderFile);
	std::cout << "inShaderVertexSource : " << glGetError() << std::endl;
	std::string fragmentSource = GetSource(fragmentShaderFile);
	std::cout << "inShaderFragmentSource : " << glGetError() << std::endl;
	GLuint vertexShaderID = CompileShader(vertexSource.c_str(), GL_VERTEX_SHADER);
	std::cout << "inShaderVertexCompile : " << glGetError() << std::endl;
	GLuint fragmentShaderID = CompileShader(fragmentSource.c_str(), GL_FRAGMENT_SHADER);
	std::cout << "inShaderFragmentCompile : " << glGetError() << std::endl;

	GLuint programID = CreateProgram(vertexShaderID, fragmentShaderID);
	std::cout << "inShaderCreateProgram : " << glGetError() << std::endl;

	glDeleteShader(vertexShaderID);
	glDeleteShader(fragmentShaderID);


	return programID;
}