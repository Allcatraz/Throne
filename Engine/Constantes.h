#pragma once

#include "glm\glm.hpp"
#include "glm\gtx\transform.hpp"
#include "glm\gtc\type_ptr.hpp"

const int TAILLE_WINDOW_X = 1024;
const int TAILLE_WINDOW_Y = 768;

#define GRAVITY -10

//#define PARTICLE_SIZE_PIXEL 0.03125
//#define PARTICLE_SIZE_PIXEL 0.25
//#define PARTICLE_SIZE_PIXEL 0.5
#define PARTICLE_SIZE_PIXEL 1.0



const glm::vec3 axeX(1.0f, 0.0f, 0.0f);
const glm::vec3 axeY(0.0f, 1.0f, 0.0f);
const glm::vec3 axeZ(0.0f, 0.0f, 1.0f);