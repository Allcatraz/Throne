#pragma once
#include "Texture.h"

class RenderedObject;

class RenderedObjectPart
{


public:
	RenderedObjectPart();
	RenderedObjectPart(E::Texture * texture, RenderedObject * object);
	~RenderedObjectPart();


	E::Texture * texture;
	RenderedObject * object;
};

