#include "stdafx.h"
#include "Transformation.h"


Transformation::Transformation()
{
	modelMatrix = glm::mat4(1.0);
}

Transformation::Transformation(glm::vec3 & pos, glm::vec3 & rotation, glm::vec3 & scaling)
{
	CreateModelMatrix(pos, rotation, scaling);
}


Transformation::~Transformation()
{
}


void Transformation::CreateModelMatrix(glm::vec3 & pos, glm::vec3& rotation, glm::vec3& scaling)
{
	modelMatrix = glm::mat4(1.0);
	Translate(pos);
	Rotate(rotation);
	Scale(scaling);
}

void Transformation::Translate(glm::vec3 & pos)
{
	modelMatrix = glm::translate(modelMatrix, pos);
	position += pos;
	modelMatrixUpdated = true;
}

void Transformation::Rotate(glm::vec3 & rotation)
{
	modelMatrix = glm::rotate(modelMatrix, rotation.x, (glm::vec3)axeX);
	modelMatrix = glm::rotate(modelMatrix, rotation.y, (glm::vec3)axeY);
	modelMatrix = glm::rotate(modelMatrix, rotation.z, (glm::vec3)axeZ);
	this->rotation += rotation;
	modelMatrixUpdated = true;
}

void Transformation::Rotate(glm::vec3 & rotation, glm::vec3 & rotationPoint)
{
	modelMatrix = glm::translate(modelMatrix, -rotationPoint);
	Rotate(rotation);
	modelMatrix = glm::translate(modelMatrix, rotationPoint);
	this->rotation += rotation;
}

void Transformation::Scale(glm::vec3 & scaling)
{
	modelMatrix = glm::scale(modelMatrix, scaling);
	this->scaling *= scaling;
	modelMatrixUpdated = true;
}

void Transformation::SetNewPosition(glm::vec3 & pos)
{
	Translate(pos - position);
}

void Transformation::SetNewRotation(glm::vec3 & rotation)
{
	Rotate(rotation - this->rotation);
}

void Transformation::SetNewScaling(glm::vec3 & scaling)
{
	Scale(scaling / this->scaling);
}


