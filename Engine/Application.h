#pragma once
#include "stdafx.h"
#include <SFML/Graphics.hpp>
#include <string>
#include "Camera.h"
#include "ParticlesContainer.h"
#include "World.h"
#include "Cube.h"
#include "CubeWorld.h"
#include "DynamicArray.hpp"
#include "Scene.h"

class Application
{
private:

	void ManageInput();
	void Draw();
	void Update();


	sf::RenderWindow window;

#pragma region Gestion du temps
	sf::Clock clock;
	float gameTime; // Variable de temps, chaque mouvement doit �tre multipli� par cette variable

	int frames = 0;
	float timeSinceLastFpsCount = 0;

	inline void FPSCounter();

	int totalFrame = 0;
	int nombreUpdate = 0;
#pragma endregion

#pragma region Camera
	Vector2i lastMousePosition;
	inline void GererCamera();
	//Camera camera = Camera(glm::vec3(-13.5, 5, 5), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	//Camera camera = Camera(glm::vec3(128, 250, 128), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	//Camera camera = Camera(glm::vec3(64, 150, 64), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	//Camera camera = Camera(glm::vec3(630, 700, 495), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	//Camera camera = Camera(glm::vec3(630, 300, 495), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	Camera camera = Camera(glm::vec3(0, 0, -3), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
#pragma endregion


#pragma region Shaders
	void LoadShaderOpenGL(E::Shader * shader, const std::string & vertPath, const std::string & fragPath);
	E::Shader * LoadShaderOpenGL(const std::string & vertPath, const std::string & fragPath);
	E::Shader * shaderParticles;
	E::Shader * shaderWorld;
	E::Shader * shaderCube;
#pragma endregion



	glm::mat4 view = glm::mat4(1.0);
	glm::mat4 projection = glm::mat4(1.0);
	glm::mat4 identity = glm::mat4(1.0);




	ParticlesContainer * container;

	bool cameraActivated = true;

	World * world;

	CubeWorld * cubeAssembly;


	Scene * scene;

public:

	int Run();

	Application(unsigned int tailleWindowX, unsigned int tailleWindowY, std::string titre);

	~Application();
};

