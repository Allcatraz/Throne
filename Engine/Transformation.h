#pragma once
class Transformation
{
public:
	Transformation();
	Transformation(glm::vec3& pos, glm::vec3& rotation, glm::vec3& scaling);
	~Transformation();

	void CreateModelMatrix(glm::vec3& pos, glm::vec3& rotation, glm::vec3& scaling);

	void SetNewPosition(glm::vec3& pos);
	void SetNewRotation(glm::vec3& rotation);
	void SetNewScaling(glm::vec3& scaling);

	void Translate(glm::vec3& pos);
	void Rotate(glm::vec3& rotation);
	void Rotate(glm::vec3& rotation, glm::vec3& rotationPoint);
	void Scale(glm::vec3& scaling);


	glm::mat4 modelMatrix = glm::mat4(1.0f);
	glm::vec3 position;
	glm::vec3 rotation;
	glm::vec3 scaling = glm::vec3(1, 1, 1);

	glm::vec3 origin;

	bool modelMatrixUpdated = false;
};

