#pragma once
#include "MatVBO.h"
#include "Instanced.h"
namespace E
{
	class InstancedMatVBO : public MatVBO, public Instanced
	{
	public:
		InstancedMatVBO();
		InstancedMatVBO(GLuint _location, int _dataSize, GLenum _dataType, int _numberOfLines, int _dataTypeSizeInBytes, int _matTypeSizeInByte, int _divisor = 1);
		~InstancedMatVBO();

		void BindToVAO() override;
	};
}


