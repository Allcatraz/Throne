#pragma once
#include "Texture.h"
class TextureReference
{
public:
	TextureReference();
	TextureReference(E::Texture * texture, int * index);
	~TextureReference();


	E::Texture * texture;
	int numberOfUsage = 0;
	int * index;
};

