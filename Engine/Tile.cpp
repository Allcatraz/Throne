#include "stdafx.h"
#include "Tile.h"


Tile::Tile()
{
}


Tile::~Tile()
{

}

Tile Tile::Create(float x, float z, float size, float res, float multiplier)
{
	Tile tile;

	int xValues[6] = { 0,	0,		size,	size,	size,	0 };
	int zValues[6] = { 0,	size,	0,		0,		size,	size };
	int staticTexturesCoords[12]{ 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1 };

	for (int i = 0, k = 0; i < 18; i += 3, k++)
	{
		float genX = x + xValues[k];
		float genZ = z + zValues[k];
		tile.vertices[i] = genX;
		tile.vertices[i + 1] = (Utility::Get2DPerlinNoiseValue(genX, genZ, res) + 1) * 0.5 * multiplier;
		tile.vertices[i + 2] = genZ;
	}

	for (int i = 0; i < 12; i++)
	{
		tile.textureCoords[i] = staticTexturesCoords[i];
	}
	return tile;
}
