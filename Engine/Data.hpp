#pragma once
template<typename T>
struct Data
{
public:
	Data();
	Data(T pointer, int size);
	~Data();


	int size;
	T * data;
};

template<typename T>
inline Data<T>::Data(T pointer, int _size) : size(_size), data(pointer)
{
}
