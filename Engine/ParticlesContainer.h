#pragma once
#include "Particule.h"
#include "Model.h"
#include "Texture.h"

class ParticlesContainer
{
public:
	ParticlesContainer(GLuint _programID, std::string & texturePath, int numberOfRowsTexture, int numberOfColumsTexture, 
		glm::vec3 & position, GLenum _sFactor, GLenum _dFactor, float _emitTime, int _maxNumberOfParticle, int _alphaValueDivider);

	~ParticlesContainer();


	void Update(float gameTime, glm::vec3 & eyePosition, glm::vec3 & viewPoint);
	void Draw();
	void AddParticule();
	
	int GetNumberOfParticles();
	int GetSize();
private:
	std::vector<Particule> particles;
	GLuint programID;

	GLuint locationNumberOfRows;
	GLuint locationNumberOfColums;

	E::Texture texture;
	sf::Image textureParticule;
	int numberOfRows = 0;
	int numberOfColumns = 0;

	glm::vec3 basePosition;

	void inline AddParticuleFast();

	GLenum sFactor;
	GLenum dFactor;

	float emitTime;
	float timeSinceLastParticleAdd = 0;

	int maxNumberOfParticle;
	

	float * matrices;
	float * texOffset;
	float * alphaValue;
	int count = 0;

	float alphaValueDivider;

	Model model;
};

