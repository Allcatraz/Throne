#pragma once
#include "Texture.h"
#include "VBO.h"
#include "Model.h"
#include "Transformation.h"

class GameObject
{
public:
	GameObject();
	GameObject(glm::vec3& pos, glm::vec3& rotation, glm::vec3& scaling);
	~GameObject();


	virtual void Draw() = 0;

	E::Texture * texture;
	E::Shader * shader;

	Transformation transformation;

	E::VBO * modelMatrixVBO;
	std::vector<Model*> * allModels;

	bool modelIsCreated = false;	
	
	virtual void CreateModel() = 0;

protected:
	void PreDraw();
};

