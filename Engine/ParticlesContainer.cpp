#include "stdafx.h"
#include "ParticlesContainer.h"

using namespace glm;


static int sizeOfFloatInBytes = sizeof(float);
static int sizeOfMat4InBytes = sizeof(mat4);
static int sizeOfVec2InBytes = sizeof(vec2);


ParticlesContainer::ParticlesContainer(GLuint _shaderProgramID, std::string & texturePath, int numberOfRowsTexture, int numberOfColumsTexture,
	glm::vec3 & position, GLenum _sFactor, GLenum _dFactor, float _emitTime, int _maxNumberOfParticle, int _alphaValueDivider) : programID(_shaderProgramID),
	basePosition(position), numberOfRows(numberOfRowsTexture), numberOfColumns(numberOfColumsTexture), sFactor(_sFactor), dFactor(_dFactor), emitTime(_emitTime),
	maxNumberOfParticle(_maxNumberOfParticle), alphaValueDivider(_alphaValueDivider)
{

	locationNumberOfRows = glGetUniformLocation(programID, "numberOfRows");
	locationNumberOfColums = glGetUniformLocation(programID, "numberOfColums");

	texture.LoadWithImagePath(texturePath);


#pragma region Model
	std::vector<float> vertices(18);

	double sizeDivBy2 = PARTICLE_SIZE_PIXEL / 2.0;

	vertices[0] = -sizeDivBy2;
	vertices[1] = sizeDivBy2;
	vertices[2] = 0;

	vertices[3] = sizeDivBy2;
	vertices[4] = sizeDivBy2;
	vertices[5] = 0;

	vertices[6] = -sizeDivBy2;
	vertices[7] = -sizeDivBy2;
	vertices[8] = 0;


	vertices[9] = sizeDivBy2;
	vertices[10] = sizeDivBy2;
	vertices[11] = 0;

	vertices[12] = sizeDivBy2;
	vertices[13] = -sizeDivBy2;
	vertices[14] = 0;

	vertices[15] = -sizeDivBy2;
	vertices[16] = -sizeDivBy2;
	vertices[17] = 0;

	std::vector<float> texCoords(12);
	float textureCoordsTemp[] =
	{
		0.0, 0.0  ,   1.0, 0.0   ,   0.0, 1.0,
		1.0, 0.0  ,   1.0, 1.0   ,   0.0, 1.0
	};

	for (int i = 0; i < 12; i++)
	{
		texCoords[i] = textureCoordsTemp[i];
	}


#pragma endregion

	model.GenVAO();

	model.GenVBO(vertices.data(), vertices.size(), GL_STATIC_DRAW, 0, 3, false);
	model.numberOfVertex = vertices.size() / 3;
	model.GenVBO(texCoords.data(), texCoords.size(), GL_STATIC_DRAW, 1, 2, false);

	model.GenVBO(2, 2, GL_FLOAT, true);
	model.GenVBO(3, 1, GL_FLOAT, true);

	model.GenMatVBO(4, 4, GL_FLOAT, 4, sizeOfFloatInBytes, sizeOfMat4InBytes, true);

	model.BindAllVBO();


	matrices = new float[maxNumberOfParticle * 16];
	texOffset = new float[maxNumberOfParticle * 2];
	alphaValue = new float[maxNumberOfParticle];
}


ParticlesContainer::~ParticlesContainer()
{
	delete[] matrices;
	delete[] texOffset;
	delete[] alphaValue;
}

void ParticlesContainer::Update(float gameTime, glm::vec3 & eyePosition, glm::vec3 & viewPoint)
{
	int index = 0;
	int index2 = 0;
	count = particles.size();
	for (int i = 0; i < count;)
	{
		Particule * p = &particles.at(i);
		p->elapsedTime += gameTime;

		if (p->elapsedTime >= p->lifeLength)
		{
			particles.erase(particles.begin() + i);
			count--;
		}
		else
		{
			//p->velocity.y += GRAVITY * gameTime;
			vec3 change = vec3(p->velocity);

			change *= gameTime;
			p->position += change;

			// Animation
			float lifeFactor = p->elapsedTime / p->lifeLength;
			int stageCount = numberOfRows * numberOfRows;
			float atlasProgression = lifeFactor * stageCount;
			int index1 = atlasProgression;


			int column = index1 % numberOfRows;
			int row = index1 / numberOfRows;
			texOffset[index2++] = (float)column / numberOfRows;
			texOffset[index2++] = (float)row / numberOfRows;

			alphaValue[i] = (1.0 - lifeFactor) / alphaValueDivider;


			vec3 forward = normalize(eyePosition - viewPoint);
			vec3 right = cross(vec3(0, 1, 0), forward);
			vec3 up = cross(forward, right);

			matrices[index++] = right.x;
			matrices[index++] = right.y;
			matrices[index] = right.z;
			index+=2;

			matrices[index++] = up.x;
			matrices[index++] = up.y;
			matrices[index] = up.z;
			index += 2;

			matrices[index++] = forward.x;
			matrices[index++] = forward.y;
			matrices[index] = forward.z;
			index += 2;

			matrices[index++] = p->position.x;
			matrices[index++] = p->position.y;
			matrices[index] = p->position.z;
			index += 2;


			i++;
		}
	}

	timeSinceLastParticleAdd += gameTime;
	if (timeSinceLastParticleAdd >= emitTime && count < maxNumberOfParticle)
	{
		AddParticuleFast();
		timeSinceLastParticleAdd = 0;
		index += 3;
		matrices[index] = 0;
		index += 4;
		matrices[index] = 0;
		index += 4;
		matrices[index] = 0;
		index += 4;
		matrices[index] = 1;
		index++;
	}
}

void ParticlesContainer::Draw()
{
	glUseProgram(programID);


	model.SetDataInVBO(2, texOffset, count, sizeOfVec2InBytes, GL_STREAM_DRAW);
	model.SetDataInVBO(3, alphaValue, count, sizeOfFloatInBytes, GL_STREAM_DRAW);
	model.SetDataInVBO(4, matrices, count, sizeOfMat4InBytes, GL_STREAM_DRAW);

	glBlendFunc(sFactor, dFactor);
	glUniform1f(locationNumberOfRows, numberOfRows);
	glUniform1f(locationNumberOfColums, numberOfColumns);

	glBindVertexArray(model.vaoID);

	texture.BindTextureToShader("inTexture", programID, 0);

	glDrawArraysInstanced(GL_TRIANGLES, 0, 6, count);
}

void ParticlesContainer::AddParticule()
{
	float randFloat = (float)rand() / RAND_MAX;
	randFloat /= 5;
	float posX = basePosition.x + (randFloat);
	float posY = basePosition.y;
	float posZ = basePosition.z;
	vec3 pos = vec3(posX, posY, posZ);

	float velocityX = (0.001 * (rand() % 10)) - 0.005;
	float velocityY = 0.005 * (rand() % 100);
	float velocityZ = (0.001 * (rand() % 10)) - 0.005;



	vec3 velocity = vec3(velocityX, velocityY, velocityZ);

	particles.push_back(Particule(pos, velocity, 0, 0, 0, (rand() % 2) + 2));
}

void inline ParticlesContainer::AddParticuleFast()
{
	particles.push_back(Particule(
		vec3(basePosition.x + (((float)rand() / RAND_MAX) / 5), basePosition.y, basePosition.z),
		vec3((0.001 * (rand() % 10)) - 0.005, 0.005 * (rand() % 100), (0.001 * (rand() % 10)) - 0.005),
		0,
		0,
		0,
		((rand() % 2) + 2)
		)
		);
}



int ParticlesContainer::GetNumberOfParticles()
{
	return particles.size();
}

int ParticlesContainer::GetSize()
{
	return particles.size();
}
