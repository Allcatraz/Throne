#pragma once
#include "Model.h"
#include "Texture.h"
#include "Tile.h"


class World
{
public:
	World();
	World(GLuint _programID);
	World(GLuint _programID, bool isInstanced);
	~World();

	void Draw();
	void DrawInstanced();


private:
	Model model;
	int numberOfTile = 0;
	int sizeX = 0;
	int sizeZ = 0;
	GLuint programID;
	GLuint textureHandle;

	std::vector<E::Texture> textures;

	std::vector<GLuint> texturesLocations;


	void _CreateMap();
	void _CreateMapInstanced();

	int GetTextureValue(float posX, float posY, int res, int multiplier);
	int GetTextureValue(float noiseValue);





	void CreateMap();
	void Create();
	Tile** CreateTiles(float res, float multiplier);
	void SetTexturesIndexsLines(Tile ** tiles, int x, int z, float res, float multiplier);
	void CreateMesh(Tile ** tiles, float res, float multiplier);
};

