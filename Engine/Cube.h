#pragma once
#include "Model.h"
#include "Texture.h"
#include "GameObject.h"

class Cube : public GameObject
{
public:
	Cube();
	Cube(glm::vec3& pos, glm::vec3& angle, glm::vec3& scaling, bool createModel);
	~Cube();

	void Draw() override;

	static const int numberOfVerticesPerCubeFace = 18;
	static const int numberOfFaceInCube = 6;
	static const int numberOfVerticesInCube = numberOfVerticesPerCubeFace * numberOfFaceInCube;

	static const int numberOfTexCoordsPerCubeFace = 12;
	static const int numberOfTexCoordsInCube = numberOfTexCoordsPerCubeFace * numberOfFaceInCube;


	void CreateModel() override;
};



