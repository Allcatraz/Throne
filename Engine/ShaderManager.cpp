#include "stdafx.h"
#include "ShaderManager.h"


std::vector<E::Shader*> ShaderManager::allShaders;

ShaderManager::ShaderManager()
{
}


ShaderManager::~ShaderManager()
{
}

void ShaderManager::LoadShaderOpenGL(E::Shader * shader, const std::string & vertPath, const std::string & fragPath)
{
	shader = LoadShaderOpenGL(vertPath, fragPath);
}

E::Shader * ShaderManager::LoadShaderOpenGL(const std::string & vertPath, const std::string & fragPath)
{
	E::Shader * shader = new E::Shader(vertPath, fragPath);
	allShaders.push_back(shader);
	return shader;
}

void ShaderManager::SetProjectionMatrix(float * ptr)
{
	for (E::Shader * s : allShaders)
	{
		std::cout << "Erreur avant setProjMatrix in ShaderManager : " << glGetError() << std::endl;
		s->SetProjectionMatrix(ptr);
		std::cout << "Erreur Apr�s setProjMatrix in ShaderManager : " << glGetError() << std::endl;
	}
}

void ShaderManager::SetViewMatrix(float * ptr)
{
	for (E::Shader * s : allShaders)
	{
		s->SetViewMatrix(ptr);
	}
}
