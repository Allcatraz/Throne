#pragma once
class Particule
{
public:
	Particule();
	Particule(glm::vec3 _position, glm::vec3 _velocity, float gravityEffect, float rotation, float scale, float lifeLength);
	~Particule();

	glm::vec3 position;
	glm::vec3 velocity;
	float gravityEffect;
	float rotation;
	float scale;
	float lifeLength;

	float elapsedTime = 0;
	

	GLuint vaoID;
	std::vector<GLuint> vboBuffer;

	glm::vec2 texOffset;
	
	float alphaValue = 0;


};

