#pragma once
namespace E
{
	class Instanced abstract
	{
	public:
		void SetDivisor(int value) { divisor = value; };
	protected:
		int divisor = 1;
	};
}


