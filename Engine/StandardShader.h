#pragma once
#include "Shader.h"

class StandardShader : public E::Shader
{
public:
	StandardShader();
	StandardShader(E::Shader * shader);
	~StandardShader();


	// Vertex Shader
	GLuint lightPositionLocation;

	// Fragment Shader
	GLuint lightColorLocation;
	GLuint shineDamperLocation;
	GLuint reflectivityLocation;
	GLuint ambientLightingLocation;

	void SetLightPosition(glm::vec3& lightPosition);
	void SetLightColor(glm::vec3& lightColor);
	void SetShineDamper(float shineDamper);
	void SetReflectivity(float reflectivity);
	void SetAmbientLighting(float ambientLighting);
};

