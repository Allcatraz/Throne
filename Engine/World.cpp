#include "stdafx.h"
#include "World.h"


using glm::rotate;
using glm::mat4;
using glm::vec3;
using glm::translate;

World::World()
{
}

World::World(GLuint _programID)
{
	programID = _programID;

	textures.push_back(E::Texture("Textures/Grass.png"));
	textures.push_back(E::Texture("Textures/Rocks.png"));
	textures.push_back(E::Texture("Textures/Snow.png"));

	sizeX = 1000;
	sizeZ = 1000;
	numberOfTile = sizeX * sizeZ;
	//CreateMap(); // 6230ms
	//Create(); // 3305ms
	_CreateMap(); // 1128ms

	texturesLocations.push_back(glGetUniformLocation(programID, "inTexture[0]"));
	texturesLocations.push_back(glGetUniformLocation(programID, "inTexture[1]"));
	texturesLocations.push_back(glGetUniformLocation(programID, "inTexture[2]"));
}

World::World(GLuint _programID, bool isInstanced)
{
	programID = _programID;

	textures.push_back(E::Texture("Textures/Grass.png"));
	textures.push_back(E::Texture("Textures/Rocks.png"));
	textures.push_back(E::Texture("Textures/Snow.png"));

	sizeX = 20;
	sizeZ = 20;
	numberOfTile = sizeX * sizeZ;
	//CreateMap(); // 6230ms
	//Create(); // 3305ms
	if (isInstanced)
	{
		_CreateMapInstanced();
	}
	else
	{
		_CreateMap(); // 1128ms
	}
	

	texturesLocations.push_back(glGetUniformLocation(programID, "inTexture[0]"));
	texturesLocations.push_back(glGetUniformLocation(programID, "inTexture[1]"));
	texturesLocations.push_back(glGetUniformLocation(programID, "inTexture[2]"));
}




void World::Draw()
{
	glEnable(GL_CULL_FACE);
	glUseProgram(programID);
	glBindVertexArray(model.vaoID);

	E::Texture::BindTextureArrayToShader("inTexture", programID, textures);

	glDrawArrays(GL_TRIANGLES, 0, 6 * numberOfTile);
	glDisable(GL_CULL_FACE);
}

void World::DrawInstanced()
{
	glUseProgram(programID);
	glBindVertexArray(model.vaoID);

	E::Texture::BindTextureArrayToShader("inTexture", programID, textures);

	glDrawArraysInstanced(GL_TRIANGLES, 0, 6, numberOfTile);
}





void World::_CreateMap()
{
	int numberOfVerticesPerTile = 18;
	int numberOfTextureCoordsPerTile = 12;
	int numberOfTextureIndexPerTile = 6;

	int sizeVertices = numberOfVerticesPerTile * numberOfTile;
	int sizeTextureCoords = numberOfTextureCoordsPerTile * numberOfTile;
	int sizeTextureIndex = numberOfTextureIndexPerTile * numberOfTile;

	int indexVertices = 0;
	int indexTexturesCoords = 0;
	int indexTextureIndex = 0;


	float * texturesCoords = new float[sizeTextureCoords];
	float * vertices = new float[sizeVertices];
	int * textureIndex = new int[sizeTextureIndex];


	int staticTexturesCoords[12]{
		0, 0, 
		0, 1, 
		1, 0, 
		0, 1, 
		1, 1, 
		1, 0 };

	int xValues[6] = { 0, 0, 1, 0, 1, 1 };
	int zValues[6] = { 0, 1, 0, 1, 1, 0 };


	int multiplier = 50; ////////////////////////// HARDCODE
	int res = 200;
	for (int z = 0; z < sizeZ; z++)
	{
		for (int x = 0; x < sizeX; x++)
		{

			for (int i = 0, k = 0; i < numberOfVerticesPerTile; i += 3, k++)
			{
				float genX = x + xValues[k];
				float genZ = z + zValues[k];
				vertices[indexVertices + i] = genX;
				//vertices[indexVertices + i + 1] = (Utility::Get2DPerlinNoiseValue(genX, genZ, res) + 1) * 0.5 * multiplier;
				vertices[indexVertices + i + 1] = ((Utility::Get2DPerlinNoiseValue(genX, genZ, res) + 1) * 0.5 * multiplier) - (multiplier / 2.0f);
				vertices[indexVertices + i + 2] = genZ;
			}

			for (int i = 0; i < numberOfTextureCoordsPerTile; i++)
			{
				texturesCoords[indexTexturesCoords + i] = staticTexturesCoords[i];
			}

			int textureValue = GetTextureValue(vertices[indexVertices + 1]);
			for (int i = 0; i < numberOfTextureIndexPerTile; i++)
			{
				textureIndex[indexTextureIndex + i] = textureValue;
			}

			indexVertices += numberOfVerticesPerTile;
			indexTexturesCoords += numberOfTextureCoordsPerTile;
			indexTextureIndex += numberOfTextureIndexPerTile;
		}
	}


	int numberOfTextureIndexLinePerSquare = 18;
	int sizeTextureIndexLine = numberOfTextureIndexLinePerSquare * numberOfTile;
	int indexTextureIndexLine = 0;

	int * textureIndexLine1 = new int[sizeTextureIndexLine];
	int * textureIndexLine2 = new int[sizeTextureIndexLine];
	int * textureIndexLine3 = new int[sizeTextureIndexLine];


	int tilePosition = 0;
	int lineSize = sizeX * numberOfTextureIndexPerTile;


	for (int z = 0; z < sizeZ; z++)
	{
		for (int x = 0; x < sizeX; x++)
		{
			for (int k = 0; k < numberOfTextureIndexLinePerSquare; k += 3)
			{
				for (int i = 0, xValue = -1; i < 3; i++, xValue++)
				{
					int xValueFOIStilePosition = xValue * numberOfTextureIndexPerTile;

					int index = (tilePosition - lineSize) + xValueFOIStilePosition;
					textureIndexLine1[indexTextureIndexLine + i + k] = index > -1 && index < sizeTextureIndex ? textureIndex[index] : GetTextureValue(x + xValue, z - 1, res, multiplier);
					
					index = tilePosition + xValueFOIStilePosition;
					textureIndexLine2[indexTextureIndexLine + i + k] = index > -1 && index < sizeTextureIndex ? textureIndex[index] : GetTextureValue(x + xValue, z, res, multiplier);
					
					index = (tilePosition + lineSize) + xValueFOIStilePosition;
					textureIndexLine3[indexTextureIndexLine + i + k] = index > -1 && index < sizeTextureIndex ? textureIndex[index] : GetTextureValue(x + xValue, z + 1, res, multiplier);
				}
			}
			indexTextureIndexLine += numberOfTextureIndexLinePerSquare;
			tilePosition += numberOfTextureIndexPerTile;
		}
	}

	model.GenVAO();

	model.GenVBO(vertices, sizeVertices, GL_STATIC_DRAW, 0, 3, false);
	model.numberOfVertex = sizeVertices / 3;
	model.GenVBO(texturesCoords, sizeTextureCoords, GL_STATIC_DRAW, 1, 2, false);

	model.GenVBO(textureIndex, sizeTextureIndex, GL_STATIC_DRAW, 2, 1, false);
	
	model.GenVBO(textureIndexLine1, sizeTextureIndexLine, GL_STATIC_DRAW, 3, 3, false);
	model.GenVBO(textureIndexLine2, sizeTextureIndexLine, GL_STATIC_DRAW, 4, 3, false);
	model.GenVBO(textureIndexLine3, sizeTextureIndexLine, GL_STATIC_DRAW, 5, 3, false);

	model.BindAllVBO();



	delete[] texturesCoords;
	delete[] vertices;
	delete[] textureIndex;
	delete[] textureIndexLine1;
	delete[] textureIndexLine2;
	delete[] textureIndexLine3;
}

void World::_CreateMapInstanced()
{
	int numberOfTextureIndexPerTile = 1;
	int numberOfIndexInModelMatrix = 16;

	int sizeTextureIndex = numberOfTextureIndexPerTile * numberOfTile;
	int sizeModelMatrix = numberOfIndexInModelMatrix * numberOfTile;

	int indexTextureIndex = 0;
	int indexModelMatrix = 0;

	int * textureIndex = new int[sizeTextureIndex];
	float * modelMatrix = new float[sizeModelMatrix];

	int multiplier = 255;
	int res = 200;

	mat4 glmModelMatrix;
	vec3 axeX(1, 0, 0);
	vec3 axeY(0, 1, 0);
	vec3 axeZ(0, 0, 1);

	float rotationX = 0;
	float rotationY = 0;
	float rotationZ = 0;

	for (int z = 0; z < sizeZ; z++)
	{
		for (int x = 0; x < sizeX; x++)
		{
			vec3 position(x + 0.5f, 0, z + 0.5f);
			vec3 pos(position.x, 0, z);
			vec3 oldVecX(pos);
			pos.y = (Utility::Get2DPerlinNoiseValue(pos.x, pos.z, res) + 1) * 0.5 * multiplier;
			vec3 newVecX(position - pos);

			rotationX = Utility::AngleEntre2Vecteurs(oldVecX, newVecX);

			vec3 pos2(x, 0, position.z);
			vec3 oldVecZ(pos);
			pos.y = (Utility::Get2DPerlinNoiseValue(pos.x, pos.z, res) + 1) * 0.5 * multiplier;
			vec3 newVecZ(position - pos);

			rotationZ = Utility::AngleEntre2Vecteurs(oldVecZ, newVecZ);

			glmModelMatrix = mat4(1.0);
			glmModelMatrix = rotate(glmModelMatrix, rotationX, axeX);
			glmModelMatrix = rotate(glmModelMatrix, rotationZ, axeZ);
			glmModelMatrix = translate(glmModelMatrix, position);


			int c = 0;
			for (int i = 0; i < 4; i++)
			{
				for (int k = 0; k < 4; k++, c++)
				{
					modelMatrix[indexModelMatrix + c] = glmModelMatrix[i][k];
				}
			}


			/*modelMatrix[indexModelMatrix + 0] = 1;
			modelMatrix[indexModelMatrix + 1] = 0;
			modelMatrix[indexModelMatrix + 2] = 0;
			modelMatrix[indexModelMatrix + 3] = 0;

			modelMatrix[indexModelMatrix + 4] = 0;
			modelMatrix[indexModelMatrix + 5] = 1;
			modelMatrix[indexModelMatrix + 6] = 0;
			modelMatrix[indexModelMatrix + 7] = 0;

			modelMatrix[indexModelMatrix + 8] = 0;
			modelMatrix[indexModelMatrix + 9] = 0;
			modelMatrix[indexModelMatrix + 10] = 1;
			modelMatrix[indexModelMatrix + 11] = 0;

			modelMatrix[indexModelMatrix + 12] = position.x;
			modelMatrix[indexModelMatrix + 13] = (Utility::Get2DPerlinNoiseValue(position.x, position.z, res) + 1) * 0.5 * multiplier;
			modelMatrix[indexModelMatrix + 14] = position.z;
			modelMatrix[indexModelMatrix + 15] = 1;*/


			textureIndex[indexTextureIndex] = GetTextureValue(modelMatrix[indexModelMatrix + 13]);

			indexModelMatrix += numberOfIndexInModelMatrix;
			indexTextureIndex += numberOfTextureIndexPerTile;
		}
	}


	int numberOfTextureIndexLinePerSquare = 3;
	int sizeTextureIndexLine = numberOfTextureIndexLinePerSquare * numberOfTile;
	int indexTextureIndexLine = 0;

	int * textureIndexLine1 = new int[sizeTextureIndexLine];
	int * textureIndexLine2 = new int[sizeTextureIndexLine];
	int * textureIndexLine3 = new int[sizeTextureIndexLine];


	int tilePosition = 0;
	int lineSize = sizeX * numberOfTextureIndexPerTile;


	for (int z = 0; z < sizeZ; z++)
	{
		for (int x = 0; x < sizeX; x++)
		{
			for (int k = 0; k < numberOfTextureIndexLinePerSquare; k += 3)
			{
				for (int i = 0, xValue = -1; i < 3; i++, xValue++)
				{
					int xValueFOIStilePosition = xValue * numberOfTextureIndexPerTile;

					int index = (tilePosition - lineSize) + xValueFOIStilePosition;
					textureIndexLine1[indexTextureIndexLine + i + k] = index > -1 && index < sizeTextureIndex ? textureIndex[index] : GetTextureValue(x + xValue + 0.5f, z - 1 + 0.5f, res, multiplier);

					index = tilePosition + xValueFOIStilePosition;
					textureIndexLine2[indexTextureIndexLine + i + k] = index > -1 && index < sizeTextureIndex ? textureIndex[index] : GetTextureValue(x + xValue + 0.5f, z + 0.5f, res, multiplier);

					index = (tilePosition + lineSize) + xValueFOIStilePosition;
					textureIndexLine3[indexTextureIndexLine + i + k] = index > -1 && index < sizeTextureIndex ? textureIndex[index] : GetTextureValue(x + xValue + 0.5f, z + 1 + 0.5f, res, multiplier);
				}
			}
			indexTextureIndexLine += numberOfTextureIndexLinePerSquare;
			tilePosition += numberOfTextureIndexPerTile;
		}
	}


	std::vector<float> vertices(18);

	double sizeDivBy2 = 0.5f;

	vertices[0] = -sizeDivBy2;
	vertices[1] = 0;
	vertices[2] = sizeDivBy2;

	vertices[3] = -sizeDivBy2;
	vertices[4] = 0;
	vertices[5] = -sizeDivBy2;

	vertices[6] = sizeDivBy2;
	vertices[7] = 0;
	vertices[8] = sizeDivBy2;


	vertices[9] = sizeDivBy2;
	vertices[10] = 0;
	vertices[11] = sizeDivBy2;

	vertices[12] = sizeDivBy2;
	vertices[13] = 0;
	vertices[14] = -sizeDivBy2;

	vertices[15] = -sizeDivBy2;
	vertices[16] = 0;
	vertices[17] = -sizeDivBy2;

	std::vector<float> texCoords(12);
	float textureCoordsTemp[] =
	{
		0.0, 0.0  ,   0.0, 1.0   ,   1.0, 0.0,
		1.0, 0.0  ,   1.0, 1.0   ,   0.0, 1.0
	};

	for (int i = 0; i < 12; i++)
	{
		texCoords[i] = textureCoordsTemp[i];
	}

	model.GenVAO();

	model.GenVBO(vertices.data(), vertices.size(), GL_STATIC_DRAW, 0, 3, false);
	model.numberOfVertex = vertices.size() / 3;
	model.GenVBO(texCoords.data(), texCoords.size(), GL_STATIC_DRAW, 1, 2, false);

	model.GenVBO(textureIndex, sizeTextureIndex, GL_STATIC_DRAW, 2, 1, true);

	model.GenVBO(textureIndexLine1, sizeTextureIndexLine, GL_STATIC_DRAW, 3, 3, true);
	model.GenVBO(textureIndexLine2, sizeTextureIndexLine, GL_STATIC_DRAW, 4, 3, true);
	model.GenVBO(textureIndexLine3, sizeTextureIndexLine, GL_STATIC_DRAW, 5, 3, true);

	model.GenMatVBO(6, 4, GL_FLOAT, 4, sizeof(float), sizeof(glm::mat4), true);
	model.SetDataInVBO(6, modelMatrix, numberOfTile, sizeof(float), GL_STATIC_DRAW);

	model.BindAllVBO();



	delete[] textureIndex;
	delete[] textureIndexLine1;
	delete[] textureIndexLine2;
	delete[] textureIndexLine3;
	delete[] modelMatrix;
}

int World::GetTextureValue(float posX, float posZ, int res, int multiplier)
{
	return GetTextureValue(Utility::Get2DPerlinNoiseValue(posX, posZ, res) + 1) * 0.5 * multiplier;
}

int World::GetTextureValue(float noiseValue)
{
	if (noiseValue > 150)
	{
		return 2;
	}
	else if (noiseValue > 110)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}






#pragma region CreateMap2
//void World::Create()
//{
//	float res = 150;
//	float multiplier = 255;
//	Tile** tiles = CreateTiles(res, multiplier);
//	CreateMesh(tiles, res, multiplier);
//	for (int z = 0; z < sizeZ; z++)
//	{
//		delete[] tiles[z];
//	}
//	delete[] tiles;
//	int a = 0;
//}
//
//Tile** World::CreateTiles(float res, float multiplier)
//{
//	Tile ** tiles = new Tile*[sizeZ];
//	float tileSize = 1;
//	for (int z = 0; z < sizeZ; z++)
//	{
//		tiles[z] = new Tile[sizeX];
//		for (int x = 0; x < sizeX; x++)
//		{
//			tiles[z][x] = Tile::Create(x, z, tileSize, res, multiplier);
//			tiles[z][x].textureIndex = GetTextureValue(x, z, res, multiplier);
//		}
//	}
//	return tiles;
//}
//
//void World::SetTexturesIndexsLines(Tile ** tiles, int x, int z, float res, float multiplier)
//{
//	for (int k = 0; k < 18; k += 3)
//	{
//		for (int i = 0, xValue = -1; i < 3; i++, xValue++)
//		{
//			if (xValue > -1 && xValue < sizeX)
//			{
//				tiles[z][x].textureIndexLine1[i + k] = z - 1 > -1 ? tiles[z - 1][x + xValue].textureIndex : GetTextureValue(x + xValue, z - 1, res, multiplier);
//				tiles[z][x].textureIndexLine2[i + k] = tiles[z][x + xValue].textureIndex;
//				tiles[z][x].textureIndexLine3[i + k] = z + 1 < sizeZ ? tiles[z + 1][x + xValue].textureIndex : GetTextureValue(x + xValue, z + 1, res, multiplier);
//			}
//			else
//			{
//				tiles[z][x].textureIndexLine1[i + k] = GetTextureValue(x + xValue, z - 1, res, multiplier);
//				tiles[z][x].textureIndexLine2[i + k] = GetTextureValue(x + xValue, z, res, multiplier);
//				tiles[z][x].textureIndexLine3[i + k] = GetTextureValue(x + xValue, z + 1, res, multiplier);
//			}
//		}
//	}
//}
//
//void World::CreateMesh(Tile ** tiles, float res, float multiplier)
//{
//	int numberOfVerticesPerTile = 18;
//	int numberOfTextureCoordsPerTile = 12;
//	int numberOfTextureIndexPerTile = 6;
//	int numberOfTextureIndexLinePerSquare = 18;
//	
//	int sizeVertices = numberOfVerticesPerTile * numberOfSquare;
//	int sizeTextureCoords = numberOfTextureCoordsPerTile * numberOfSquare;
//	int sizeTextureIndex = numberOfTextureIndexPerTile * numberOfSquare;
//	int sizeTextureIndexLine = numberOfTextureIndexLinePerSquare * numberOfSquare;
//
//	int indexVertices = 0;
//	int indexTexturesCoords = 0;
//	int indexTextureIndex = 0;
//	int indexTextureIndexLine = 0;
//
//
//	float * vertices = new float[sizeVertices];
//	float * texturesCoords = new float[sizeTextureCoords];
//	int * textureIndex = new int[sizeTextureIndex];
//
//	int * textureIndexLine1 = new int[sizeTextureIndexLine];
//	int * textureIndexLine2 = new int[sizeTextureIndexLine];
//	int * textureIndexLine3 = new int[sizeTextureIndexLine];
//
//
//	for (int z = 0; z < sizeZ; z++)
//	{
//		for (int x = 0; x < sizeX; x++)
//		{
//			for (int i = 0; i < numberOfVerticesPerTile; i++)
//			{
//				vertices[indexVertices + i] = tiles[z][x].vertices[i];
//				if (i < numberOfTextureCoordsPerTile)
//				{
//					texturesCoords[indexTexturesCoords + i] = tiles[z][x].textureCoords[i];
//				}
//				if (i < numberOfTextureIndexPerTile)
//				{
//					textureIndex[indexTextureIndex + i] = tiles[z][x].textureIndex;
//				}
//			}
//
//			SetTexturesIndexsLines(tiles, x, z, res, multiplier);
//			for (int k = 0; k < numberOfTextureIndexLinePerSquare; k += 3)
//			{
//				for (int i = 0, xValue = -1; i < 3; i++, xValue++)
//				{
//					textureIndexLine1[indexTextureIndexLine + i + k] = tiles[z][x].textureIndexLine1[i + k];
//					textureIndexLine2[indexTextureIndexLine + i + k] = tiles[z][x].textureIndexLine2[i + k];
//					textureIndexLine3[indexTextureIndexLine + i + k] = tiles[z][x].textureIndexLine3[i + k];
//				}
//			}
//			indexVertices += numberOfVerticesPerTile;
//			indexTexturesCoords += numberOfTextureCoordsPerTile;
//			indexTextureIndex += numberOfTextureIndexPerTile;
//			indexTextureIndexLine += numberOfTextureIndexLinePerSquare;
//		}
//	}
//
//
//	model = new Model(vertices, texturesCoords, textureIndex, textureIndexLine1, textureIndexLine2, textureIndexLine3,
//		sizeVertices, sizeTextureCoords, sizeTextureIndex, sizeTextureIndexLine, GL_STATIC_DRAW);
//
//	delete[] vertices;
//	delete[] texturesCoords;
//	delete[] textureIndex;
//	delete[] textureIndexLine1;
//	delete[] textureIndexLine2;
//	delete[] textureIndexLine3;
//}
#pragma endregion

#pragma region old
//void World::CreateMap()
//{
//	int sizeTextureCoords = 12 * numberOfSquare;
//	int sizeVertices = 18 * numberOfSquare;
//	int sizeTextureIndex = 6 * numberOfSquare;
//	int sizeTextureIndexLine = 18 * numberOfSquare;
//
//	int indexVertices = 0;
//	int indexTexturesCoords = 0;
//	int indexTextureIndex = 0;
//	int indexTextureIndexLine = 0;
//
//
//
//	float * texturesCoords = new float[sizeTextureCoords];
//	float * vertices = new float[sizeVertices];
//	int * textureIndex = new int[sizeTextureIndex];
//
//	int * textureIndexLine1 = new int[sizeTextureIndexLine];
//	int * textureIndexLine2 = new int[sizeTextureIndexLine];
//	int * textureIndexLine3 = new int[sizeTextureIndexLine];
//
//	int staticTexturesCoords[12]{ 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1 };
//
//	int xValues[6] = { 0, 0, 1, 1, 1, 0 };
//	int zValues[6] = { 0, 1, 0, 0, 1, 1 };
//
//	for (int x = 0; x < sizeX; x++)
//	{
//		for (int z = 0; z < sizeZ; z++)
//		{
//			int res = 150;
//			int multiplier = 255;
//
//			for (int i = 0, k = 0; i < 18; i += 3, k++)
//			{
//				float genX = x + xValues[k];
//				float genZ = z + zValues[k];
//				vertices[indexVertices + i] = genX;
//				vertices[indexVertices + i + 1] = (Utility::Get2DPerlinNoiseValue(genX, genZ, res) + 1) * 0.5 * multiplier;
//				vertices[indexVertices + i + 2] = genZ;
//			}
//
//			for (int i = 0; i < 12; i++)
//			{
//				texturesCoords[indexTexturesCoords + i] = staticTexturesCoords[i];
//			}
//
//
//			int textureIndexValue = GetTextureValue(x, z, res, multiplier);
//
//			for (int i = 0; i < 6; i++)
//			{
//				textureIndex[indexTextureIndex + i] = textureIndexValue;
//			}
//
//			for (int k = 0; k < 18; k += 3)
//			{
//				for (int i = 0, xValue = -1; i < 3; i++, xValue++)
//				{
//					textureIndexLine1[indexTextureIndexLine + i + k] = GetTextureValue(x + xValue, z - 1, res, multiplier);
//					textureIndexLine2[indexTextureIndexLine + i + k] = GetTextureValue(x + xValue, z, res, multiplier);
//					textureIndexLine3[indexTextureIndexLine + i + k] = GetTextureValue(x + xValue, z + 1, res, multiplier);
//				}
//			}
//
//
//
//			indexVertices += 18;
//			indexTexturesCoords += 12;
//			indexTextureIndex += 6;
//			indexTextureIndexLine += 18;
//		}
//	}
//	model = new Model(vertices, texturesCoords, textureIndex, textureIndexLine1, textureIndexLine2, textureIndexLine3,
//		sizeVertices, sizeTextureCoords, sizeTextureIndex, sizeTextureIndexLine, GL_STATIC_DRAW);
//
//	delete[] texturesCoords;
//	delete[] vertices;
//	delete[] textureIndex;
//	delete[] textureIndexLine1;
//	delete[] textureIndexLine2;
//	delete[] textureIndexLine3;
//}
#pragma endregion




World::~World()
{
	
}


