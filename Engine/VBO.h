#pragma once

namespace E
{
	class VBO
	{
	public:
		VBO();
		VBO(GLuint _location, int _dataSize, GLenum _dataType);
		~VBO();

		template<typename T>
		void SetDataInVBO(T * data, int count, int dataTypeSizeInBytes, GLenum drawType);

		virtual void BindToVAO();



		GLuint location;
		int dataSize;
		GLuint id;
		GLenum dataType;
	};

	template<typename T>
	inline void VBO::SetDataInVBO(T * data, int count, int dataTypeSizeInBytes, GLenum drawType)
	{
		glBindBuffer(GL_ARRAY_BUFFER, id);
		glBufferData(GL_ARRAY_BUFFER, dataTypeSizeInBytes * count, data, drawType);
	}
}


