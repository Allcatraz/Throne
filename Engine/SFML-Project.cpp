// Projet-SFML.cpp�: d�finit le point d'entr�e pour l'application console.
//

#include "stdafx.h"
#include "Application.h"
#include "Constantes.h"

#define _SCL_SECURE_NO_WARNINGS
#pragma warning(disable:4996)  

int main()
{
	try
	{
		Application * app = new Application(TAILLE_WINDOW_X, TAILLE_WINDOW_Y, "Window");
		app->Run();
		delete app;
	}
	catch (const std::exception& e)
	{
		std::cout << "Une erreur non attrap�e est surevenue dans le programme : " << e.what() << std::endl;
		system("pause");
	}
	return 0;
}
