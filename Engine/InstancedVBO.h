#pragma once
#include "VBO.h"
#include "Instanced.h"
namespace E
{
	class InstancedVBO : public VBO, public Instanced
	{
	public:
		InstancedVBO();
		InstancedVBO(GLuint _location, int _dataTypeSizeInBytes, GLenum _dataType, int _divisor = 1);
		~InstancedVBO();

		void BindToVAO() override;
	};
}


