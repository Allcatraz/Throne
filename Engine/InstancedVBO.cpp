#include "stdafx.h"
#include "InstancedVBO.h"


E::InstancedVBO::InstancedVBO()
{
}

E::InstancedVBO::InstancedVBO(GLuint _location, int _dataSize, GLenum _dataType, int _divisor) : VBO(_location, _dataSize, _dataType)
{
	divisor = _divisor;
}


E::InstancedVBO::~InstancedVBO()
{
}

void E::InstancedVBO::BindToVAO()
{
	VBO::BindToVAO();
	glVertexAttribDivisor(location, divisor);
}

