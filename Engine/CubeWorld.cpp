#include "stdafx.h"
#include "CubeWorld.h"
#include "VBO.h"

CubeWorld::CubeWorld()
{
	texture.LoadWithImagePath("Textures/Grass.png");
}


CubeWorld::~CubeWorld()
{

}

void CubeWorld::SetMesh()
{
	float * vertices = new float[cubeCount * 108];
	float * texCoords = new float[cubeCount * 72];
	int verticesPosition = 0;
	int texCoordsPosition = 0;
	std::vector<float> texCoordsTemp = CubeData::GetStaticTexCoords();
	for (int z = 0; z < sizeZ; z++)
	{
		for (int y = 0; y < sizeY; y++)
		{
			for (int x = 0; x < sizeX; x++)
			{
				std::vector<float> verticesTemp = CubeData::GetStaticVertices(glm::vec3(x, y, z));
				memcpy(&vertices[verticesPosition], &verticesTemp[0], 108);
				verticesPosition += 108;
				memcpy(&texCoords[texCoordsPosition], &texCoordsTemp[0], 108);
				texCoordsPosition += 72;
			}
		}
	}
	model = new Model(vertices, texCoords, cubeCount * 108, cubeCount * 72, GL_STATIC_DRAW);
	delete[] vertices;
	delete[] texCoords;
}

void CubeWorld::SetInstanced()
{
	std::vector<float> vertices = CubeData::GetStaticVertices();
	std::vector<float> texCoords = CubeData::GetStaticTexCoords();
	float modelArray[12] = { 1.0f , 0.0f , 0.0f , 0.0f , 0.0f , 1.0f , 0.0f , 0.0f , 0.0f , 0.0f , 1.0f , 0.0f };
	float * modelMatrix = new float[cubeCount * 16];
	int indexModelMatrix = 0;
	for (int z = 0; z < sizeZ; z++)
	{
		for (int y = 0; y < sizeY; y++)
		{
			for (int x = 0; x < sizeX; x++)
			{
				memcpy(&modelMatrix[indexModelMatrix], modelArray, 12 * sizeof(float));
				modelMatrix[indexModelMatrix + 12] = x;
				modelMatrix[indexModelMatrix + 13] = y;
				modelMatrix[indexModelMatrix + 14] = z;
				modelMatrix[indexModelMatrix + 15] = 1.0f;

				indexModelMatrix += 16;
			}
		}
	}
	model = new Model();

	model->GenVAO();

	model->GenVBO(vertices.data(), 108, GL_STATIC_DRAW, 0, 3, false);
	model->numberOfVertex = 108 / 3;
	model->GenVBO(texCoords.data(), 72, GL_STATIC_DRAW, 1, 2, false);

	model->GenMatVBO(2, 4, GL_FLOAT, 4, sizeof(float), sizeof(glm::mat4), true);
	model->SetDataInVBO(2, modelMatrix, cubeCount * 16, sizeof(float), GL_STATIC_DRAW);

	model->BindAllVBO();
}

void CubeWorld::SetInstanced4()
{
	//
	float multiplier = 255;
	int res = 200;
	float ** heightMap = new float*[sizeZ];
	for (int z = 0; z < sizeZ; z++)
	{
		heightMap[z] = new float[sizeX];
		for (int x = 0; x < sizeX; x++)
		{
			heightMap[z][x] = (Utility::Get2DPerlinNoiseValue(x, z, res) + 1) * 0.5f * multiplier;
		}
	}
	// 7ms


	//
	cubes = new CubeData**[sizeZ];
	for (int z = 0; z < sizeZ; z++)
	{
		cubes[z] = new CubeData*[sizeY];
		for (int y = 0; y < sizeY; y++)
		{
			cubes[z][y] = new CubeData[sizeX];
			for (int x = 0; x < sizeX; x++)
			{
				if (y > heightMap[z][x])
				{
					cubes[z][y][x].type = CubeData::Type::Air;
					cubes[z][y][x].isTextured = false;
					cubes[z][y][x].isTransparent = true;
				}
			}
		}
	}
	//400ms -> 72ms 

	for (int z = 0; z < sizeZ; z++)
	{
		delete[] heightMap[z];
	}
	delete[] heightMap;

	//
	SetFacesToRender(0, sizeX, 0, sizeY, 0, sizeZ);
	// 337ms -> 107ms -> 46ms

	//
	for (int i = 0; i < 6; i++)
	{
		allFaces[i].dataSize = 3;
		allFaces[i].Resize(numberOfFacesToDraw[i]);
	}

	int indexPositions[CubeData::numberOfFaceInCube] = { 0, 0, 0, 0, 0, 0 };

	for (int z = 0; z < sizeZ; z++)
	{
		for (int y = 0; y < sizeY; y++)
		{
			for (int x = 0; x < sizeX; x++)
			{
				if (cubes[z][y][x].isTextured == true)
				{
					for (int i = 0; i < CubeData::numberOfFaceInCube; i++)
					{
						if (cubes[z][y][x].facesToRender[i] == true)
						{
							allFaces[i].data[indexPositions[i] + 0] = x;
							allFaces[i].data[indexPositions[i] + 1] = y;
							allFaces[i].data[indexPositions[i] + 2] = z;

							allFaces[i].allId[indexPositions[i] / 3] = cubes[z][y][x].id;

							indexPositions[i] += 3;
						}
					}
				}

			}
		}
	}
	//220ms -> 39


	//
	E::VBO * texCoordsVBO = Model::StaticGenVBO(CubeData::GetStaticTexCoords1Face(), CubeData::numberOfTexCoordsPerCubeFace, GL_STATIC_DRAW, 1, 2, false);

	for (int i = 0; i < CubeData::numberOfFaceInCube; i++)
	{
		facesPositionsVBO[i] = Model::StaticGenVBO(allFaces[i].data.data(), numberOfFacesToDraw[i] * 3, GL_STREAM_DRAW, 2, 3, true);
	}


	for (int i = 0; i < CubeData::numberOfFaceInCube; i++)
	{
		Model * model = new Model();
		model->GenVAO();
		model->GenVBO(CubeData::GetStaticVerticesInArray((CubeData::Faces)i), CubeData::numberOfVerticesPerCubeFace, GL_STATIC_DRAW, 0, 3, false);
		model->numberOfVertex = CubeData::numberOfVerticesPerCubeFace / 3;
		model->AddVBO(texCoordsVBO);
		model->AddVBO(facesPositionsVBO[i]);
		model->BindAllVBO();
		allModels.push_back(model);
	}
	// 15ms
}


void CubeWorld::DrawMesh()
{
	glUseProgram(shaderID);
	texture.BindTextureToShader("inTexture", shaderID, 0);
	glBindVertexArray(model->vaoID);
	glDrawArrays(GL_TRIANGLES, 0, cubeCount * 36);
}

void CubeWorld::DrawInstanced()
{
	glUseProgram(shaderID);
	texture.BindTextureToShader("inTexture", shaderID, 0);
	glBindVertexArray(model->vaoID);
	glDrawArraysInstanced(GL_TRIANGLES, 0, 36, cubeCount);
}

void CubeWorld::DrawInstanced4()
{
	glUseProgram(shaderID);
	texture.BindTextureToShader("inTexture", shaderID, 0);
	for (int i = 0; i < allModels.size(); i++)
	{
		glBindVertexArray(allModels.at(i)->vaoID);
		glDrawArraysInstanced(GL_TRIANGLES, 0, 6, numberOfFacesToDraw[i]);
	}
}

void CubeWorld::SetFacesToRender(int minX, int maxX, int minY, int maxY, int minZ, int maxZ)
{
	for (int z = minZ; z < maxZ; z++)
	{
		for (int y = minY; y < maxY; y++)
		{
			for (int x = minX; x < maxX; x++)
			{
				if (cubes[z][y][x].isTextured == true)
				{
					if (z - 1 > -1 && cubes[z - 1][y][x].isTransparent == true)
					{
						cubes[z][y][x].facesToRender[0] = true;
						numberOfFacesToDraw[0]++;
					}
					if (x + 1 < sizeX && cubes[z][y][x + 1].isTransparent == true)
					{
						cubes[z][y][x].facesToRender[1] = true;
						numberOfFacesToDraw[1]++;
					}
					if (z + 1 < sizeZ && cubes[z + 1][y][x].isTransparent == true)
					{
						cubes[z][y][x].facesToRender[2] = true;
						numberOfFacesToDraw[2]++;
					}
					if (x - 1 > -1 && cubes[z][y][x - 1].isTransparent == true)
					{
						cubes[z][y][x].facesToRender[3] = true;
						numberOfFacesToDraw[3]++;
					}
					if (y + 1 < sizeY && cubes[z][y + 1][x].isTransparent == true)
					{
						cubes[z][y][x].facesToRender[4] = true;
						numberOfFacesToDraw[4]++;
					}
					if (y - 1 > -1 && cubes[z][y - 1][x].isTransparent == true)
					{
						cubes[z][y][x].facesToRender[5] = true;
						numberOfFacesToDraw[5]++;
					}
				}
			}
		}
	}
}

void CubeWorld::Update(glm::vec3& newPosition)
{
	glm::vec3 diff = actualPosition - newPosition;
	if (abs(diff.x) > numberOfBlockDifferenceBeforeUpdate ||
		abs(diff.z) > numberOfBlockDifferenceBeforeUpdate)
	{
		// Delete -> Decalage -> Generation

		float multiplier = 255;
		int res = 200;
		glm::tvec3<int> diffInt = abs(diff);
		// bouge vers la droite
		if (diff.x < 0)
		{
			// DELETE
			for (int z = 0; z < sizeZ; z++)
			{
				for (int y = 0; y < sizeY; y++)
				{
					for (int x = 0; x < diffInt.x; x++)
					{
						if (cubes[z][y][x].isTextured == true)
						{
							for (int i = 0; i < CubeData::numberOfFaceInCube; i++)
							{
								if (cubes[z][y][x].facesToRender[i] == true)
								{
									allFaces[i].Erase(cubes[z][y][x].id);
									numberOfFacesToDraw[i]--;
								}
							}
						}
					}
				}
			} // 7ms


			
			// DECALAGE
			int copySize = (sizeX - diffInt.x) * sizeof(CubeData);
			for (int z = 0; z < sizeZ; z++)
			{
				for (int y = 0; y < sizeY; y++)
				{
					memcpy(cubes[z][y], cubes[z][y] + diffInt.x, copySize);
				}
			} // 47ms

			// GENERATION
 			float ** heightMap = new float*[sizeZ];
			for (int z = 0; z < sizeZ; z++)
			{
				heightMap[z] = new float[diffInt.x];
				for (int x = 0; x < diffInt.x; x++)
				{
					heightMap[z][x] = (Utility::Get2DPerlinNoiseValue((int)actualPosition.x + (sizeX / 2) + x, z, res) + 1) * 0.5f * multiplier;
				}
			}

			for (int z = 0; z < sizeZ; z++)
			{
				for (int y = 0; y < sizeY; y++)
				{
					for (int x = sizeX - diffInt.x, x2 = 0; x < sizeX; x++, x2++)
					{
						cubes[z][y][x] = CubeData::CubeData();
						if (y > heightMap[z][x2])
						{
							cubes[z][y][x].type = CubeData::Type::Air;
							cubes[z][y][x].isTextured = false;
							cubes[z][y][x].isTransparent = true;
						}
					}
				}
			} // 9ms

			SetFacesToRender(sizeX - diffInt.x, sizeX, 0, sizeY, 0, sizeZ);

			float * pos = new float[3];
			for (int z = 0; z < sizeZ; z++)
			{
				for (int y = 0; y < sizeY; y++)
				{
					for (int x = sizeX - diffInt.x, k = 0; x < sizeX; x++, k++)
					{
						if (cubes[z][y][x].isTextured == true)
						{
							for (int i = 0; i < CubeData::numberOfFaceInCube; i++)
							{
								if (cubes[z][y][x].facesToRender[i] == true)
								{
									pos[0] = (int)actualPosition.x + (sizeX / 2) + k;
									pos[1] = y;
									pos[2] = z;
									allFaces[i].Insert(cubes[z][y][x].id, pos);
								}
							}
						}
					}
				}
			}
			delete[] pos; // 6ms

			for (int i = 0; i < CubeData::numberOfFaceInCube; i++)
			{
				facesPositionsVBO[i]->SetDataInVBO(allFaces[i].data.data(), numberOfFacesToDraw[i] * 3, sizeof(float), GL_STREAM_DRAW);
			}
		}
		// bouge vers la gauche
		else
		{

		}

		// bouge vers back
		if (diff.z < 0)
		{

		}

		// bouge vers front
		else
		{

		}
		actualPosition = newPosition;
	}
}




