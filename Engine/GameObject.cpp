#include "stdafx.h"
#include "GameObject.h"


GameObject::GameObject()
{
}

GameObject::GameObject(glm::vec3 & pos, glm::vec3 & rotation, glm::vec3 & scaling)
{
	transformation.CreateModelMatrix(pos, rotation, scaling);
}


GameObject::~GameObject()
{
}

void GameObject::PreDraw()
{
	shader->Use();
	texture->BindTextureToShader("inTexture", shader->programID, 0);

	if (transformation.modelMatrixUpdated)
	{
		transformation.modelMatrixUpdated = false;
		modelMatrixVBO->SetDataInVBO(glm::value_ptr(transformation.modelMatrix), 16, sizeof(float), GL_STREAM_DRAW);
	}
}