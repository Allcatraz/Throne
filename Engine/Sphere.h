#pragma once
#include "GameObject.h"
class Sphere : public GameObject
{
public:
	Sphere();
	Sphere(glm::vec3& pos, glm::vec3& angle, glm::vec3& scaling, float rayon, bool createModel);
	~Sphere();

	float rayon = 0;
	int pointCount = 0;


	void CreateModel() override;
	void CreatePoints(std::vector<glm::vec3>& points, int couches, float precision);
	void CreateVertices(std::vector<float>& vertices, std::vector<glm::vec3>& points, int couches);
	void CreateColors(std::vector<float>& colors, int numberOfPoints);

	void Draw() override;
};

