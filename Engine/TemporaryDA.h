#pragma once
class TemporaryDA
{
public:
	TemporaryDA();
	~TemporaryDA();
	std::vector<int> allId;
	std::vector<float> data;
	int dataSize = 0;
	int IdSearch(int id);
	void Resize(int size);
	void Erase(int id);
	void Insert(int id, float * data);
	int SearchInsertIndex(int id);
private:
	int ComputeNewIndex(int max, int min);
};

