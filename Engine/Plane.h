#pragma once
#include "GameObject.h"
class Plane : public GameObject
{
public:
	Plane();
	Plane(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale, bool createModel);
	~Plane();

	void Draw() override;
	void CreateModel() override;
};

