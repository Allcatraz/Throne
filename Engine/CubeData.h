#pragma once

class CubeData
{
public:
	enum Faces
	{
		front,
		right,
		back,
		left,
		up,
		down
	};
	enum Type
	{
		Air,
		Grass
	};

	Type type = Type::Grass;
	bool isTransparent = false;
	bool isTextured = true;
	bool facesToRender[6] = { false, false, false, false, false, false };
	int id;
	CubeData();

#pragma region STATIC
static std::vector<float> GetStaticVertices();
static std::vector<float> GetStaticVertices(Faces face);
static std::vector<float> GetStaticVertices(glm::vec3 position);


static float * GetStaticVerticesInArray();
static float * GetStaticVerticesInArray(Faces face);
static float * GetStaticVerticesInArray(Faces face, int triangleIndex);
static float * GetStaticVerticesInArray(glm::vec3 position);


static std::vector<float> GetStaticTexCoords();
static float * GetStaticTexCoords1Face();
static float * GetStaticTexCoords1Face(int triangle);

static const int numberOfVerticesPerCubeFace = 18;
static const int numberOfFaceInCube = 6;
static const int numberOfVerticesInCube = numberOfVerticesPerCubeFace * numberOfFaceInCube;

static const int numberOfTexCoordsPerCubeFace = 12;
static const int numberOfTexCoordsInCube = numberOfTexCoordsPerCubeFace * numberOfFaceInCube;
#pragma endregion
};



