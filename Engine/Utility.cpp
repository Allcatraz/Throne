#include "stdafx.h"
#include "Utility.h"
#include <math.h>

#pragma region Static attributes
static unsigned int perm[512] =
{ 151,160,137,91,90,15,131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,
142,8,99,37,240,21,10,23,190,6,148,247,120,234,75,0,26,197,62,94,252,219,
203,117,35,11,32,57,177,33,88,237,149,56,87,174,20,125,136,171,168,68,175,
74,165,71,134,139,48,27,166,77,146,158,231,83,111,229,122,60,211,133,230,220,
105,92,41,55,46,245,40,244,102,143,54,65,25,63,161,1,216,80,73,209,76,132,
187,208,89,18,169,200,196,135,130,116,188,159,86,164,100,109,198,173,186,3,
64,52,217,226,250,124,123,5,202,38,147,118,126,255,82,85,212,207,206,59,227,
47,16,58,17,182,189,28,42,223,183,170,213,119,248,152,2,44,154,163,70,221,
153,101,155,167,43,172,9,129,22,39,253,19,98,108,110,79,113,224,232,178,185,
112,104,218,246,97,228,251,34,242,193,238,210,144,12,191,179,162,241,81,51,145,
235,249,14,239,107,49,192,214,31,181,199,106,157,184,84,204,176,115,121,50,45,
127,4,150,254,138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,
156,180,151,160,137,91,90,15,131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,
142,8,99,37,240,21,10,23,190,6,148,247,120,234,75,0,26,197,62,94,252,219,
203,117,35,11,32,57,177,33,88,237,149,56,87,174,20,125,136,171,168,68,175,
74,165,71,134,139,48,27,166,77,146,158,231,83,111,229,122,60,211,133,230,220,
105,92,41,55,46,245,40,244,102,143,54,65,25,63,161,1,216,80,73,209,76,132,
187,208,89,18,169,200,196,135,130,116,188,159,86,164,100,109,198,173,186,3,
64,52,217,226,250,124,123,5,202,38,147,118,126,255,82,85,212,207,206,59,227,
47,16,58,17,182,189,28,42,223,183,170,213,119,248,152,2,44,154,163,70,221,
153,101,155,167,43,172,9,129,22,39,253,19,98,108,110,79,113,224,232,178,185,
112,104,218,246,97,228,251,34,242,193,238,210,144,12,191,179,162,241,81,51,145,
235,249,14,239,107,49,192,214,31,181,199,106,157,184,84,204,176,115,121,50,45,
127,4,150,254,138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,
156,180 };


static float unit = 1.0f / sqrt(2);
static float gradient2[8][2] = { { unit,unit },{ -unit,unit },{ unit,-unit },{ -unit,-unit },{ 1,0 },{ -1,0 },{ 0,1 },{ 0,-1 } };
#pragma endregion






/// <summary>
/// Calcul de la distance entre deux points
/// </summary>
/// <param name="position1">The position1.</param>
/// <param name="position2">The position2.</param>
/// <returns>
/// Retourne la distance entre les deux points
/// </returns>
float Utility::DistanceEntreDeuxPoints(sf::Vector2f position1, sf::Vector2f position2)
{
	float x2MoinsX1 = position2.x - position1.x;
	float y2MoinsY1 = position2.y - position1.y;
	return sqrt((x2MoinsX1 * x2MoinsX1) + (y2MoinsY1 * y2MoinsY1));
}

/// <summary>
/// Calcule les cath�tes d'un triangle rectangle isoc�le
/// </summary>
/// <param name="hypothenuse">longueur l'hypoth�nuse du triagnle</param>
/// <returns>
///	Retourne la longeur d'une cath�te
/// </returns>
float Utility::CalculCathetesAvecHypothenuse(float hypothenuse)
{
	return sqrt((hypothenuse * hypothenuse) / 2);
}

/// <summary>
/// Convertit un angle en radian vers un angle en degr�
/// </summary>
/// <param name="angleRadian"> L'angle en radian </param>
/// <returns>
/// Retourne l'angle convertit
/// </returns>
float Utility::ConvertirRadianEnDegre(float angleRadian)
{
	return 180 * angleRadian / PI;
}

/// <summary>
/// Convertit un angle en degr� vers un angle en randian
/// </summary>
/// <param name="angleDegre">L'angle en radian</param>
/// <returns>
/// Retourne l'angle convertit
/// </returns>
float Utility::ConvertirDegreeEnRadian(float angleDegre)
{
	return PI * angleDegre / 180;
}


/// <summary>
/// Calcule une v�locit� selon une position et une destination.
/// La v�locit� est calcul�e selon une ligne droite
/// </summary>
/// <param name="position"> Position de l'objet </param>
/// <param name="destination"> Destination de l'objet </param>
/// <returns>
/// Retourne le vecteur de v�locit�
/// </returns>
sf::Vector2f Utility::CalculerVelocity(sf::Vector2f position, sf::Vector2f destination)
{
	float distanceEntreDeuxPoints = DistanceEntreDeuxPoints(position, destination);
	if (distanceEntreDeuxPoints == 0)
	{
		return sf::Vector2f();
	}
	return (destination - position) / distanceEntreDeuxPoints;
}

float Utility::Get2DPerlinNoiseValue(float x, float y, float res)
{
	float tempX, tempY;
	int x0, y0, ii, jj, gi0, gi1, gi2, gi3;
	float tmp, s, t, u, v, Cx, Cy, Li1, Li2;

	//Adapter pour la r�solution
	x /= res;
	y /= res;

	//On r�cup�re les positions de la grille associ�e � (x,y)
	//Convertit les positions float en int
	x0 = (int)(x);
	y0 = (int)(y);

	// Obtiens une valeur entre x0//y0 et 255
	ii = x0 & 255;
	jj = y0 & 255;

	//Pour r�cup�rer les vecteurs
	gi0 = perm[ii + perm[jj]] % 8;
	gi1 = perm[ii + 1 + perm[jj]] % 8;

	gi2 = perm[ii + perm[jj + 1]] % 8;
	gi3 = perm[ii + 1 + perm[jj + 1]] % 8;

	//on r�cup�re les vecteurs et on pond�re
	tempX = x - x0;
	tempY = y - y0;
	s = gradient2[gi0][0] * tempX + gradient2[gi0][1] * tempY;

	tempX = x - (x0 + 1);
	tempY = y - y0;
	t = gradient2[gi1][0] * tempX + gradient2[gi1][1] * tempY;

	tempX = x - x0;
	tempY = y - (y0 + 1);
	u = gradient2[gi2][0] * tempX + gradient2[gi2][1] * tempY;

	tempX = x - (x0 + 1);
	tempY = y - (y0 + 1);
	v = gradient2[gi3][0] * tempX + gradient2[gi3][1] * tempY;


	//Lissage
	tmp = x - x0;
	Cx = 3 * tmp * tmp - 2 * tmp * tmp * tmp;

	Li1 = s + Cx*(t - s);
	Li2 = u + Cx*(v - u);

	tmp = y - y0;
	Cy = 3 * tmp * tmp - 2 * tmp * tmp * tmp;

	return Li1 + Cy*(Li2 - Li1);
	//return s + t + u + v;
}

float Utility::AngleEntre2Vecteurs(glm::vec3 vec1, glm::vec3 vec2)
{

	return ProduitScalaire(vec1, vec2) / (CalculerNorme(vec1) * CalculerNorme(vec2));
}

float Utility::ProduitScalaire(glm::vec3 vec1, glm::vec3 vec2)
{
	return (vec1.x * vec2.x) + (vec1.y * vec2.y) + (vec1.z * vec2.z);
}

float Utility::CalculerNorme(glm::vec3 vec)
{
	return sqrt((vec.x * vec.x) + (vec.y * vec.y) + (vec.z * vec.z));
}

glm::vec3 Utility::ClockwiseRotate(float angle, glm::vec3 vecteur, glm::vec3 axe)
{
	return vecteur * ConstructionMatriceDeRotation3x3Clockwise(angle, axe);
}

glm::mat3 Utility::ConstructionMatriceDeRotation3x3Clockwise(float angle, glm::vec3 axe)
{
	float cosAngle = cos(ConvertirDegreeEnRadian(angle));
	float sinAngle = sin(ConvertirDegreeEnRadian(angle));

	float unMoinsCosAngle = 1 - cosAngle;

	axe = glm::normalize(axe);

	float xSin = axe.x * sinAngle;
	float ySin = axe.y * sinAngle;
	float zSin = axe.z * sinAngle;

	glm::mat3 rotation;

	rotation[0][0] = ((axe.x * axe.x) * unMoinsCosAngle) + cosAngle;
	rotation[0][1] = ((axe.x * axe.y) * unMoinsCosAngle) - zSin;
	rotation[0][2] = ((axe.x * axe.z) * unMoinsCosAngle) + ySin;

	rotation[1][0] = ((axe.x * axe.y) * unMoinsCosAngle) + zSin;
	rotation[1][1] = ((axe.y * axe.y) * unMoinsCosAngle) + cosAngle;
	rotation[1][2] = ((axe.y * axe.z) * unMoinsCosAngle) - xSin;

	rotation[2][0] = ((axe.x * axe.z) * unMoinsCosAngle) - ySin;
	rotation[2][1] = ((axe.y * axe.z) * unMoinsCosAngle) + xSin;
	rotation[2][2] = ((axe.z * axe.z) * unMoinsCosAngle) + cosAngle;

	return rotation;
}

glm::mat4 Utility::ConstructionMatriceDeRotation4x4Clockwise(float angle, glm::vec3 axe)
{
	float cosAngle = cos(ConvertirDegreeEnRadian(angle));
	float sinAngle = sin(ConvertirDegreeEnRadian(angle));

	float unMoinsCosAngle = 1 - cosAngle;

	axe = glm::normalize(axe);

	float xSin = axe.x * sinAngle;
	float ySin = axe.y * sinAngle;
	float zSin = axe.z * sinAngle;

	glm::mat4 rotation;

	rotation[0][0] = ((axe.x * axe.x) * unMoinsCosAngle) + cosAngle;
	rotation[0][1] = ((axe.x * axe.y) * unMoinsCosAngle) - zSin;
	rotation[0][2] = ((axe.x * axe.z) * unMoinsCosAngle) + ySin;
	rotation[0][3] = 0;

	rotation[1][0] = ((axe.x * axe.y) * unMoinsCosAngle) + zSin;
	rotation[1][1] = ((axe.y * axe.y) * unMoinsCosAngle) + cosAngle;
	rotation[1][2] = ((axe.y * axe.z) * unMoinsCosAngle) - xSin;
	rotation[1][3] = 0;

	rotation[2][0] = ((axe.x * axe.z) * unMoinsCosAngle) - ySin;
	rotation[2][1] = ((axe.y * axe.z) * unMoinsCosAngle) + xSin;
	rotation[2][2] = ((axe.z * axe.z) * unMoinsCosAngle) + cosAngle;
	rotation[2][3] = 0;

	rotation[3][0] = 0;
	rotation[3][1] = 0;
	rotation[3][2] = 0;
	rotation[3][3] = 1;

	return rotation;
}

bool Utility::CheckFilePathExtension(std::string & filePath, std::string & extension)
{
	for (int i = extension.length() - 1; i >= 0; i--)
	{
		if (filePath[filePath.length() - 1 - (extension.length() - 1 - i)] != extension[i])
		{
			return false;
		}
	}
	if (filePath[filePath.length() - 1 - extension.length()] != '.')
	{
		return false;
	}
	return true;
}

std::vector<std::string> Utility::SplitString(std::string & str, char token)
{
	std::vector<std::string> tokens;

	std::_String_iterator<std::_String_val<std::_Simple_types<char>>> beginIterator = str.begin();
	std::_String_iterator<std::_String_val<std::_Simple_types<char>>> endIterator = str.begin();

	int max = str.length();
	for (int i = 0; i < max; i++)
	{
		if (str[i] != token)
		{
			endIterator++;
		}
		else
		{	
			tokens.push_back(std::string(beginIterator, endIterator));
			endIterator++;
			beginIterator = endIterator;
		}
	}
	if (endIterator != beginIterator)
	{
		tokens.push_back(std::string(beginIterator, endIterator));
	}
	return tokens;
}
