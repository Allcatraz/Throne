#include "stdafx.h"
#include "InstancedMatVBO.h"


E::InstancedMatVBO::InstancedMatVBO()
{
}

E::InstancedMatVBO::InstancedMatVBO(GLuint _location, int _dataSize, GLenum _dataType, 
	int _numberOfLines, int _dataTypeSizeInBytes, int _matTypeSizeInByte,
	int _divisor) : 
	MatVBO(_location, _dataSize, _dataType, _numberOfLines, _dataTypeSizeInBytes, _matTypeSizeInByte)
{
	divisor = _divisor;
}


E::InstancedMatVBO::~InstancedMatVBO()
{
}

void E::InstancedMatVBO::BindToVAO()
{
	glBindBuffer(GL_ARRAY_BUFFER, id);
	for (int i = 0; i < numberOfLines; i++)
	{
		AttribPointer(i);
		glVertexAttribDivisor(location + i, divisor);
	}
}
