#pragma once
class TriangleIndices
{
public:
	TriangleIndices();
	TriangleIndices(int index1, int index2, int index3, int normalIndex1, int normalIndex2, int normalIndex3, int textIndex1, int textIndex2, int textIndex3);
	~TriangleIndices();

	int index1;
	int index2;
	int index3;
	int normalIndex1;
	int normalIndex2;
	int normalIndex3;
	int textIndex1;
	int textIndex2;
	int textIndex3;
};

