#pragma once
#include <string>
#include <GL/glew.h>
#include <iostream>
#include <fstream>
#include <sstream>

namespace E
{
	class Shader
	{
	public:
		Shader();
		Shader(const std::string& vertexShaderFile, const std::string& fragmentShaderFile);
		~Shader();

		virtual void Use();

	
		GLuint LoadShader(const std::string& vertexShaderFile, const std::string& fragmentShaderFile);
		GLuint CreateProgram(GLuint vertexShaderID, GLuint fragmentShaderID);
		std::string GetSource(const std::string& sourceFile);
		GLuint CompileShader(const GLchar* source, GLenum type) const;


		void SetViewMatrix(float * ptr);
		void SetViewMatrix(glm::mat4& mat);

		void SetProjectionMatrix(float * ptr);
		void SetProjectionMatrix(glm::mat4 & mat);


		void SetMatrix3x3f(GLuint location, float * ptr);
		void SetMatrix3x3f(GLuint location, glm::mat3& mat);

		void SetMatrix4x4f(GLuint location, float * ptr);
		void SetMatrix4x4f(GLuint location, glm::mat4& mat);


		void SetFloat(GLuint location, float value);
		void SetInt(GLuint location, int value);
		void SetDouble(GLuint location, double value);


		void SetVec2Float(GLuint location, float * ptr);
		void SetVec2Float(GLuint location, glm::vec2& vec);

		void SetVec2Int(GLuint location, int * ptr);
		void SetVec2Int(GLuint location, glm::tvec2<int>& vec);

		void SetVec2Double(GLuint location, double * ptr);
		void SetVec2Double(GLuint location, glm::tvec2<double>& vec);


		void SetVec3Float(GLuint location, float * ptr);
		void SetVec3Float(GLuint location, glm::vec3& vec);

		void SetVec3Int(GLuint location, int * ptr);
		void SetVec3Int(GLuint location, glm::tvec3<int>& vec);

		void SetVec3Double(GLuint location, double * ptr);
		void SetVec3Double(GLuint location, glm::tvec3<double>& vec);


		void SetVec4Float(GLuint location, float * ptr);
		void SetVec4Float(GLuint location, glm::vec4& vec);

		void SetVec4Int(GLuint location, int * ptr);
		void SetVec4Int(GLuint location, glm::tvec4<int>& vec);

		void SetVec4Double(GLuint location, double * ptr);
		void SetVec4Double(GLuint location, glm::tvec4<double>& vec);





		GLuint programID;
	private:
		GLuint viewMatrixLocation;
		GLuint projectionMatrixLocation;


	};
}

