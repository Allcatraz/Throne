#include "stdafx.h"
#include "TemporaryDA.h"


TemporaryDA::TemporaryDA()
{
}


TemporaryDA::~TemporaryDA()
{
}

int TemporaryDA::IdSearch(int id)
{
	int index = (allId.size() - 1) / 2;
	bool notFound = true;
	int min = 0;
	int max = allId.size() - 1;
	int lastIndex = 0;
	while (notFound)
	{
		lastIndex = index;
		if (allId[index] == id)
		{
			return index;
		}
		else if (allId[index] < id)
		{
			index += ComputeNewIndex(max, min);
			min = lastIndex + 1;
		}
		else if (allId[index] > id)
		{
			index -= ComputeNewIndex(max, min);
			max = lastIndex - 1;
		}
		if (lastIndex == index)
		{
			return -1;
		}
	}
	return -1;
}

void TemporaryDA::Resize(int size)
{
	allId.resize(size);
	data.resize(size * dataSize);
}

void TemporaryDA::Erase(int id)
{
	int index = IdSearch(id);
	if (index != -1)
	{
		int indexData = index * dataSize;
		allId.erase(allId.begin() + index);
		data.erase(data.begin() + indexData, data.begin() + indexData + dataSize);
	}
	else
	{
		std::cout << "Bad id value : " << id << "\n";
	}
	
}

void TemporaryDA::Insert(int id, float * data)
{
	int index = SearchInsertIndex(id);
	int indexData = index * dataSize;
	allId.insert(allId.begin() + index, id);
	this->data.insert(this->data.begin() + indexData, data, data + dataSize);
}

int TemporaryDA::SearchInsertIndex(int id)
{
	if (allId.size() == 0)
	{
		return 0;
	}
	int index = (allId.size() - 1) / 2;
	bool notFound = true;
	int min = 0;
	int max = allId.size() - 1;
	int lastIndex = 0;
	while (notFound)
	{
		lastIndex = index;
		if (allId[index] == id)
		{
			return index;
		}
		else if (allId[index] < id)
		{
			index += ComputeNewIndex(max, min);
			min = lastIndex + 1;
		}
		else if (allId[index] > id)
		{
			index -= ComputeNewIndex(max, min);
			max = lastIndex - 1;
		}
		if (lastIndex == index)
		{
			if (allId[index] < id)
			{
				return index + 1;
			}
			else
			{
				return index;
			}

		}
	}
	return -1;
}

int TemporaryDA::ComputeNewIndex(int max, int min)
{
	float v1 = (max - min) / 4.0f;
	int v2 = v1;
	if (v1 > v2)
	{
		v2++;
	}
	return v2;
}