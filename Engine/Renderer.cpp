#include "stdafx.h"
#include "Renderer.h"


Renderer::Renderer()
{
}

Renderer::Renderer(E::Shader * shader) : shader(shader)
{
}

Renderer::Renderer(E::Shader * shader, Model * model) : shader(shader), model(model)
{	
	textureIndexVBO = Model::StaticGenVBO(3, 1, GL_INT, true);
	modelMatrixVBO = Model::StaticGenMatVBO(4, 4, GL_FLOAT, 4, sizeof(float), sizeof(glm::mat4), true);

	model->AddVBO(textureIndexVBO);
	model->AddVBO(modelMatrixVBO);
	glBindVertexArray(model->vaoID);
	textureIndexVBO->BindToVAO();
	modelMatrixVBO->BindToVAO();
}


Renderer::~Renderer()
{
}

void Renderer::Draw()
{
	shader->Use();

	for (int i = 0; i < textures.size(); i++)
	{
		textures.at(i).texture->BindTextureToShader("inTexture[" + std::to_string(i) + "]", shader->programID, i);
	}

	glBindVertexArray(model->vaoID);
	glDrawArraysInstanced(GL_TRIANGLES, 0, model->numberOfVertex, numberOfObject);
}

void Renderer::AddObject(RenderedObjectPart * objectPart)
{
	int index = 0;
	if (CheckIfTextureExist(objectPart->texture->handle, index))
	{
		textures.at(index).numberOfUsage++;
		textureIndexPtr.push_back(textures.at(index).index);
	}
	else
	{
		int * i = new int(textures.size());
		textures.push_back(TextureReference(objectPart->texture, i));
		textureIndexPtr.push_back(i);
	}

	renderedObjectsPart.push_back(objectPart);
	numberOfObject++;

	float * ptr = glm::value_ptr(objectPart->object->transformation.modelMatrix);
	modelMatrix.insert(modelMatrix.end(), ptr, ptr + 16);
	modelMatrixVBO->SetDataInVBO(modelMatrix.data(), modelMatrix.size(), sizeof(float), GL_STREAM_DRAW);

	textureIndex.push_back(*textureIndexPtr.at(textureIndexPtr.size() - 1));
	textureIndexVBO->SetDataInVBO(textureIndex.data(), textureIndex.size(), sizeof(int), GL_STREAM_DRAW);
}


void Renderer::Update()
{
	bool modelMatrixVBONeedUpdate = false;
	for (int i = 0; i < numberOfObject; i++)
	{
		if (renderedObjectsPart.at(i)->object->transformation.modelMatrixUpdated)
		{
			float * ptr = glm::value_ptr(renderedObjectsPart.at(i)->object->transformation.modelMatrix);
			std::copy(ptr, ptr + 16, modelMatrix.data() + (i * 16));
			modelMatrixVBONeedUpdate = true;
		}
	}
	if (modelMatrixVBONeedUpdate)
	{
		modelMatrixVBO->SetDataInVBO(modelMatrix.data(), modelMatrix.size(), sizeof(float), GL_STREAM_DRAW);
	}
}

void Renderer::RemoveObject(RenderedObjectPart * objectPart, bool breakAfterRemoveOnce)
{
	for (int i = 0; i < numberOfObject; i++)
	{
		if (renderedObjectsPart[i] == objectPart)
		{
			modelMatrix.erase(modelMatrix.begin() + (i * 16), modelMatrix.begin() + ((i + 1) * 16));
			int index = 0;
			CheckIfTextureExist(renderedObjectsPart[i]->texture->handle, index);
			textures[index].numberOfUsage--;
			if (textures[index].numberOfUsage == 0)
			{
				textures.erase(textures.begin() + index);
				for (int i = index; i < textures.size(); i++)
				{
					*textures.at(i).index--;
				}
				textureIndex.clear();
				textureIndex.resize(numberOfObject);
				for (int i = 0; i < numberOfObject; i++)
				{
					textureIndex[i] = *textureIndexPtr[i];
				}
			}
			textureIndexPtr.erase(textureIndexPtr.begin() + i);
			textureIndex.erase(textureIndex.begin() + i);
			renderedObjectsPart.erase(renderedObjectsPart.begin() + i);
			numberOfObject--;
			if (breakAfterRemoveOnce)
			{
				break;
			}
		}
	}
	modelMatrixVBO->SetDataInVBO(modelMatrix.data(), modelMatrix.size(), sizeof(float), GL_STREAM_DRAW);
	textureIndexVBO->SetDataInVBO(textureIndex.data(), textureIndex.size(), sizeof(int), GL_STREAM_DRAW);
}

bool Renderer::CheckIfTextureExist(GLuint textureHandle, int & index)
{
	int s = textures.size();
	for (int i = 0; i < s; i++)
	{
		if (textures.at(i).texture->handle == textureHandle)
		{
			index = i;
			return true;
		}
	}
	return false;
}
