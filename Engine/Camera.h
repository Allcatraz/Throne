//SFML
#include <SFML/Window/Event.hpp>


using namespace glm;
using namespace sf;
class Camera
{
public:
	Camera();
	Camera(vec3& position, vec3& viewPoint, vec3& verticalAxe);
	~Camera();

	void move(float timeInSeconds);
	void lookAt(mat4& modelview);
	void moveOrientation(int xRel, int yRel);

	vec3 GetPosition();
	vec3 GetViewPoint();
	float speed = 6;
private:
	float phi = 0;
	float theta = 0;
	vec3 orientation;
	vec3 verticalAxe;
	vec3 lateralMove;
	vec3 position;
	vec3 viewPoint;


	void setViewPoint(vec3 viewPoint);

};