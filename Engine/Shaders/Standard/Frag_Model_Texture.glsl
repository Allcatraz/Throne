#version 410 core

// In
in vec2 textureCoords;
in vec3 surfaceNormal;
in vec3 vertexToLight;
in vec3 vertexToCamera;

flat in int textureIndex;

uniform vec3 lightColor;
uniform float shineDamper;
uniform float reflectivity;
uniform float ambientLighting;

uniform sampler2D inTexture[32];

// Sortie 
out vec4 out_Color;




void main()
{

	vec3 normalizedNormal = normalize(surfaceNormal);
	vec3 normalizedVertexToLight = normalize(vertexToLight);
	
	float dotNormalAvecVertexToLight = dot(normalizedNormal, normalizedVertexToLight);

	float brightness = max(dotNormalAvecVertexToLight, ambientLighting);

	vec3 diffuse = brightness * lightColor;

	vec3 normalizedVertexToCamera = normalize(vertexToCamera);
	vec3 lightDirection = -normalizedVertexToLight;
	vec3 reflectedLight = reflect(lightDirection, normalizedNormal);

	float specularFactor = dot(reflectedLight, normalizedVertexToCamera);
	specularFactor = max(specularFactor, 0.0);

	float dampedFactor = pow(specularFactor, shineDamper);
	dampedFactor = max(dampedFactor, 0.0);

	vec3 finalSpecular = dampedFactor * reflectivity * lightColor;

	vec4 textureColour = texture(inTexture[textureIndex], textureCoords);
	if(textureColour.a < 0.5)
	{
		discard;
	}


	out_Color = (vec4(diffuse, 1.0) * textureColour) + vec4(finalSpecular, 1.0);
}