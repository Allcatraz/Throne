#version 410 core

// Entr�es
layout(location = 0) in vec3 in_Vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 inTextureCoords;
layout(location = 3) in int inTextureIndex;
layout(location = 4) in mat4 model;

// Uniform
uniform mat4 view;
uniform mat4 projection;
uniform vec3 lightPosition;
uniform vec3 windDirection;


// Out
out vec3 surfaceNormal;
out vec3 vertexToLight;
out vec3 vertexToCamera;
out vec2 textureCoords;
flat out int textureIndex;


void main()
{
	vec4 worldPosition = model * vec4(in_Vertex, 1.0);

	vec3 sun = vec3(worldPosition.x, worldPosition.y + 25, worldPosition.z);


	vec3 normalizedNormal = normalize((transpose(inverse(model)) * vec4(normal, 1.0)).xyz);
	surfaceNormal = normalizedNormal;
	vertexToLight = sun - worldPosition.xyz;
	vertexToCamera = (inverse(view) * vec4(0.0, 0.0, 0.0, 1.0) - worldPosition).xyz;
	
	textureCoords = inTextureCoords;

	textureIndex = inTextureIndex;



	float mouvementRate = in_Vertex.y - model[1].z;
	float dotMouvementRate = dot(normalize(windDirection), normalizedNormal);
	mouvementRate *= abs(dotMouvementRate);
	mouvementRate = pow(mouvementRate, 2);


	vec4 windForce = vec4(0.0, 0.0, 0.0, 0.0);

	windForce.x += windDirection.x * mouvementRate;
	windForce.y += windDirection.y * mouvementRate;
	windForce.z += windDirection.z * mouvementRate;

	gl_Position = projection * view * (worldPosition + windForce);
}