#version 410 core

// Uniform
uniform sampler2D inTexture;

// In
in vec2 coordTexture;
in float alpha;


// Sortie 
out vec4 out_Color;


void main()
{
	out_Color = texture(inTexture, coordTexture);
	out_Color.w *= alpha;
}