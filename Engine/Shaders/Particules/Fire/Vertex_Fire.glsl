#version 410 core

// Entr�es
layout(location = 0) in vec3 in_Vertex;
layout(location = 1) in vec2 textureCoordsInVertex;

// Uniform
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform vec2 texOffset;
uniform float blendFactor;
uniform float numberOfRows;
uniform float numberOfColums;

// Out
out vec2 coordTexture;
out float blend;


void main()
{
	vec2 textureCoords = textureCoordsInVertex;
	textureCoords.x /= numberOfColums;
	textureCoords.y /= numberOfRows;
	coordTexture = textureCoords + texOffset;

	blend = blendFactor;

	gl_Position = projection * view * model * vec4(in_Vertex, 1.0);
}