#version 410 core

// Uniform
uniform sampler2D inTexture;

// In
in vec2 coordTexture;
in float blend;


// Sortie 
out vec4 out_Color;


void main()
{
	vec4 colour1 = texture(inTexture, coordTexture);

	out_Color = texture(inTexture, coordTexture);
}