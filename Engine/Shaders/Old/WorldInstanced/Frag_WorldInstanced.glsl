#version 410 core

// In
in vec2 coordTexture;
flat in int textureIndex;
flat in ivec3 textureIndexInFragLine1;
flat in ivec3 textureIndexInFragLine2;
flat in ivec3 textureIndexInFragLine3;

// Uniform
uniform sampler2D inTexture[10];

// Sortie 
out vec4 out_Color;

//Functions
float ComputeBlend(float distance);
float ComputeDistanceEntre2Points(float x, float y);
float ProduitScalaire(vec2 vecteur1, vec2 vecteur2);
float DistancePointAvecDroite(vec2 q, vec2 p, vec2 vecDirecteur);
float Norme(vec2 vecteur);
float ComputeBlendCorner(float distance);

void main()
{
	vec4 cOut = vec4(0, 0, 0, 0);
	bool coutInitialised = false;


	//Nord
	if (textureIndexInFragLine2.y != textureIndexInFragLine1.y)
	{
		vec4 c2 = mix(
			texture(inTexture[textureIndexInFragLine2.y], coordTexture),
			texture(inTexture[textureIndexInFragLine1.y], vec2(coordTexture.x, 1 - coordTexture.y)),
			ComputeBlend(DistancePointAvecDroite(coordTexture, vec2(0.5, 0), vec2(1 - 0, 0 - 0)))
			);
		if (coutInitialised == false)
		{
			cOut = c2;
			coutInitialised = true;
		}
		else
		{
			cOut = mix(cOut, c2, 0.5);
		}
	}



	//Est
	if (textureIndexInFragLine2.y != textureIndexInFragLine2.z)
	{
		vec4 c4 = mix(
			texture(inTexture[textureIndexInFragLine2.y], coordTexture),
			texture(inTexture[textureIndexInFragLine2.z], vec2(1 - coordTexture.x, coordTexture.y)),
			ComputeBlend(DistancePointAvecDroite(coordTexture, vec2(1, 0.5), vec2(1 - 1, 1 - 0)))
			);
		if (coutInitialised == false)
		{
			cOut = c4;
			coutInitialised = true;
		}
		else
		{
			cOut = mix(cOut, c4, 0.5);
		}
	}



	//Sud
	if (textureIndexInFragLine2.y != textureIndexInFragLine3.y)
	{
		vec4 c6 = mix(
			texture(inTexture[textureIndexInFragLine2.y], coordTexture),
			texture(inTexture[textureIndexInFragLine3.y], vec2(coordTexture.x, 1 - coordTexture.y)),
			ComputeBlend(DistancePointAvecDroite(coordTexture, vec2(0.5, 1), vec2(1 - 0, 1 - 1)))
			);
		if (coutInitialised == false)
		{
			cOut = c6;
			coutInitialised = true;
		}
		else
		{
			cOut = mix(cOut, c6, 0.5);
		}
	}



	//Ouest
	if (textureIndexInFragLine2.y != textureIndexInFragLine2.x)
	{
		vec4 c8 = mix(
			texture(inTexture[textureIndexInFragLine2.y], coordTexture),
			texture(inTexture[textureIndexInFragLine2.x], vec2(1 - coordTexture.x, coordTexture.y)),
			ComputeBlend(DistancePointAvecDroite(coordTexture, vec2(0, 0.5), vec2(0 - 0, 1 - 0)))
			);
		if (coutInitialised == false)
		{
			cOut = c8;
			coutInitialised = true;
		}
		else
		{
			cOut = mix(cOut, c8, 0.5);
		}
	}

	

	//Nord ouest
	if (textureIndexInFragLine2.y != textureIndexInFragLine1.x &&
		textureIndexInFragLine2.y == textureIndexInFragLine2.x &&
		textureIndexInFragLine2.y == textureIndexInFragLine1.y)
	{
		vec4 c1 = mix(
			texture(inTexture[textureIndexInFragLine2.y], coordTexture),
			texture(inTexture[textureIndexInFragLine1.x], vec2(1, 1) - coordTexture),
			ComputeBlendCorner(DistancePointAvecDroite(coordTexture, vec2(0, 0), vec2(1 - 0, 0 - 1)))
			);
		if (coutInitialised == false)
		{
			cOut = c1;
			coutInitialised = true;
		}
		else
		{
			cOut = mix(cOut, c1, 0.5);
		}

	}

	//Nord est
	if (textureIndexInFragLine2.y != textureIndexInFragLine1.z && 
		textureIndexInFragLine2.y == textureIndexInFragLine1.y &&
		textureIndexInFragLine2.y == textureIndexInFragLine2.z)
	{
		vec4 c3 = mix(
			texture(inTexture[textureIndexInFragLine2.y], coordTexture),
			texture(inTexture[textureIndexInFragLine1.z], vec2(1, 1) - coordTexture),
			ComputeBlendCorner(DistancePointAvecDroite(coordTexture, vec2(1, 0), vec2(1, 1)))
			);
		if (coutInitialised == false)
		{
			cOut = c3;
			coutInitialised = true;
		}
		else
		{
			cOut = mix(cOut, c3, 0.5);
		}
	}

	//Sud est
	if (textureIndexInFragLine2.y != textureIndexInFragLine3.z && 
		textureIndexInFragLine2.y == textureIndexInFragLine3.y && 
		textureIndexInFragLine2.y == textureIndexInFragLine2.z)
	{
		vec4 c5 = mix(
			texture(inTexture[textureIndexInFragLine2.y], coordTexture),
			texture(inTexture[textureIndexInFragLine3.z], vec2(1, 1) - coordTexture),
			ComputeBlendCorner(DistancePointAvecDroite(coordTexture, vec2(1, 1), vec2(1 - 0, 0 - 1)))
			);
		if (coutInitialised == false)
		{
			cOut = c5;
			coutInitialised = true;
		}
		else
		{
			cOut = mix(cOut, c5, 0.5);
		}
	}

	//Sud ouest
	if (textureIndexInFragLine2.y != textureIndexInFragLine3.x &&
		textureIndexInFragLine2.y == textureIndexInFragLine3.y &&
		textureIndexInFragLine2.y == textureIndexInFragLine2.x)
	{
		vec4 c7 = mix(
			texture(inTexture[textureIndexInFragLine2.y], coordTexture),
			texture(inTexture[textureIndexInFragLine3.x], vec2(1, 1) - coordTexture),
			ComputeBlendCorner(DistancePointAvecDroite(coordTexture, vec2(0, 1), vec2(1, 1)))
			);
		if (coutInitialised == false)
		{
			cOut = c7;
			coutInitialised = true;
		}
		else
		{
			cOut = mix(cOut, c7, 0.5);
		}
	}



	if (coutInitialised == false)
	{
		cOut = texture(inTexture[textureIndexInFragLine2.y], coordTexture);
	}
	out_Color = cOut;
}


float DistancePointAvecDroite(vec2 q, vec2 p, vec2 vecDirecteur)
{
	vec2 normale = vec2(-vecDirecteur.y, vecDirecteur.x);
	return abs(ProduitScalaire((p - q), normale)) / Norme(normale);
}

float ProduitScalaire(vec2 vecteur1, vec2 vecteur2)
{
	return (vecteur1.x * vecteur2.x) + (vecteur1.y * vecteur2.y);
}

float Norme(vec2 vecteur)
{
	return sqrt((vecteur.x * vecteur.x) + (vecteur.y * vecteur.y));
}

float ComputeBlend(float distance)
{
	return (-0.5 * distance) + 0.5;
}

float ComputeBlendCorner(float distance)
{
	return (-0.3 * distance) + 0.3;
}

float ComputeDistanceEntre2Points(float x, float y)
{
	float deltaX = coordTexture.x - x;
	float deltaY = coordTexture.y - y;
	return sqrt((deltaX * deltaX) + (deltaY * deltaY));
}