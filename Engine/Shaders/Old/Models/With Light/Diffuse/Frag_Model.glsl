#version 410 core

// In
in vec4 color;
in vec3 surfaceNormal;
in vec3 vertexToLight;

// Sortie 
out vec4 out_Color;

uniform vec3 lightColor;

void main()
{
	vec3 normalizedNormal = normalize(surfaceNormal);
	vec3 normalizedVertexToLight = normalize(vertexToLight);
	
	float nDot1 = dot(normalizedNormal, normalizedVertexToLight);

	float brightness = max(nDot1, 0.0);

	vec3 diffuse = brightness * lightColor;

	out_Color = vec4(diffuse, 1.0) * color;
}