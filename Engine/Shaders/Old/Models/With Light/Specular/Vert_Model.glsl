#version 410 core

// Entr�es
layout(location = 0) in vec3 in_Vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec4 inColor;

// Uniform
uniform mat4 view;
uniform mat4 projection;
uniform mat4 model;
uniform vec3 lightPosition;


// Out
out vec4 color;
out vec3 surfaceNormal;
out vec3 vertexToLight;
out vec3 vertexToCamera;


void main()
{
	vec4 worldPosition = model * vec4(in_Vertex, 1.0);
	//surfaceNormal = model * normal;

	gl_Position = projection * view * worldPosition;

	surfaceNormal = (model * vec4(normal, 1.0)).xyz;
	vertexToLight = lightPosition - worldPosition.xyz;
	vertexToCamera = (inverse(view) * vec4(0.0, 0.0, 0.0, 1.0) - worldPosition).xyz;


	color = inColor;
}