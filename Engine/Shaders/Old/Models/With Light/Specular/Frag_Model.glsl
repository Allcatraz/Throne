#version 410 core

// In
in vec4 color;
in vec3 surfaceNormal;
in vec3 vertexToLight;
in vec3 vertexToCamera;

// Sortie 
out vec4 out_Color;

uniform vec3 lightColor;
uniform float shineDamper;
uniform float reflectivity;

void main()
{
	vec3 normalizedNormal = normalize(surfaceNormal);
	vec3 normalizedVertexToLight = normalize(vertexToLight);
	
	float nDot1 = dot(normalizedNormal, normalizedVertexToLight);

	float brightness = max(nDot1, 0.0);

	vec3 diffuse = brightness * lightColor;

	vec3 normalizedVertexToCamera = normalize(vertexToCamera);
	vec3 lightDirection = -normalizedVertexToLight;
	vec3 reflectedLight = reflect(lightDirection, normalizedNormal);

	float specularFactor = dot(reflectedLight, normalizedVertexToCamera);
	specularFactor = max(specularFactor, 0.0);

	float dampedFactor = pow(specularFactor, shineDamper);

	vec3 finalSpecular = dampedFactor * reflectivity * lightColor;

	out_Color = (vec4(diffuse, 1.0) * color) + vec4(finalSpecular, 1.0);
}