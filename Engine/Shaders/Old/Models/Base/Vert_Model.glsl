#version 410 core

// Entr�es
layout(location = 0) in vec3 in_Vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec4 inColor;

// Uniform
uniform mat4 view;
uniform mat4 projection;

// Out
out vec4 color;


void main()
{
	color = inColor;
	gl_Position = projection * view * vec4(in_Vertex, 1.0);

}