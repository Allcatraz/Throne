#version 410 core

// Entr�es
layout(location = 0) in vec3 in_Vertex;
layout(location = 1) in vec2 textureCoordsInVertex;


layout(location = 2) in int textureIndexInVertex;
layout(location = 3) in ivec3 textureIndexInVertexLine1;
layout(location = 4) in ivec3 textureIndexInVertexLine2;
layout(location = 5) in ivec3 textureIndexInVertexLine3;

// Uniform
uniform mat4 view;
uniform mat4 projection;

// Out
out vec2 coordTexture;
flat out int textureIndex;

flat out ivec3 textureIndexInFragLine1;
flat out ivec3 textureIndexInFragLine2;
flat out ivec3 textureIndexInFragLine3;


void main()
{
	textureIndex = textureIndexInVertex;
	coordTexture = textureCoordsInVertex;

	textureIndexInFragLine1 = textureIndexInVertexLine1;
	textureIndexInFragLine2 = textureIndexInVertexLine2;
	textureIndexInFragLine3 = textureIndexInVertexLine3;

	gl_Position = projection * view * vec4(in_Vertex, 1.0);

}