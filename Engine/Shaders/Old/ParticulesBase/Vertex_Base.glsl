#version 410 core

// Entr�es
layout(location = 0) in vec3 in_Vertex;
layout(location = 1) in vec2 textureCoordsInVertex;

// Instanced
layout(location = 2) in vec2 texOffset;
layout(location = 3) in float alphaValue;
layout(location = 4) in mat4 model;

// Uniform
uniform mat4 view;
uniform mat4 projection;
uniform float numberOfRows;
uniform float numberOfColums;

// Out
out vec2 coordTexture;
out float alpha;


void main()
{
	vec2 textureCoords = textureCoordsInVertex;
	textureCoords.y /= numberOfRows;
	textureCoords.x /= numberOfColums;
	coordTexture = textureCoords + texOffset;

	alpha = alphaValue;

	gl_Position = projection * view * model * vec4(in_Vertex, 1.0);
}