#version 410 core

// In
in vec2 texCoordsInFrag;


uniform sampler2D inTexture;

// Sortie 
out vec4 out_Color;


void main()
{
	out_Color = texture(inTexture, texCoordsInFrag);
}