#version 410 core

// Entr�es
layout(location = 0) in vec3 in_Vertex;
layout(location = 1) in vec2 texCoords;

// Uniform
uniform mat4 view;
uniform mat4 projection;
uniform mat4 model;

// Out
out vec2 texCoordsInFrag;


void main()
{
	texCoordsInFrag = texCoords;
	gl_Position = projection * view * model * vec4(in_Vertex, 1.0);

}