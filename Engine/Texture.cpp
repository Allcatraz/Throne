#include "stdafx.h"
#include "Texture.h"


E::Texture::Texture()
{


}

E::Texture::Texture(const std::string& path)
{
	LoadWithImagePath(path);
}

E::Texture::Texture(sf::Image& image)
{
	LoadWithImage(image);
}



E::Texture::~Texture()
{
}


void E::Texture::LoadWithImagePath(const std::string& path)
{
	handle = LoadTextureWithPath(path);
}

void E::Texture::LoadWithImage(sf::Image& image)
{
	handle = LoadTextureWithImage(image);
}


GLuint E::Texture::LoadTextureWithPath(const std::string& path)
{
	sf::Image image;
	if (image.loadFromFile(path) == false)
	{
		std::cout << "Erreur lors du chargement de la textue " << path << "\n";
		return NULL;
	}
	return LoadTextureWithImage(image);
}

GLuint E::Texture::LoadTextureWithImage(sf::Image& image)
{
	GLuint textureHandle;
	glGenTextures(1, &textureHandle);
	glBindTexture(GL_TEXTURE_2D, textureHandle);	
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.getSize().x, image.getSize().y, 0, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8_REV, image.getPixelsPtr());
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, -1.5f);
	glBindTexture(GL_TEXTURE_2D, 0);
	return textureHandle;
}

void E::Texture::BindTextureToShader(const std::string& attributeName, GLuint shaderProgramID, int index)
{
	BindTextureToShader(attributeName, shaderProgramID, handle, index);
}

void E::Texture::BindTextureArrayToShader(const std::string& attributeName, GLuint shaderProgramID, std::vector<Texture>& texturesToBind)
{
	for (int i = 0; i < texturesToBind.size(); i++)
	{
		BindTextureToShader(attributeName + "[" + std::to_string(i) + "]", shaderProgramID, texturesToBind.at(i).handle, i);
	}
}



void E::Texture::BindTextureToShader(const std::string& attributeName, GLuint shaderProgramID, GLuint textureHandle, int index)
{
	glActiveTexture(GL_TEXTURE0 + index);
	glBindTexture(GL_TEXTURE_2D, textureHandle);
	glUniform1i(glGetUniformLocation(shaderProgramID, attributeName.data()), index);
}
