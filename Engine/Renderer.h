#pragma once
#include "Shader.h"
#include "Transformation.h"
#include "Texture.h"
#include "RenderedObject.h"
#include "RenderedObjectPart.h"
#include "VBO.h"
#include "Model.h"
#include "TextureReference.h"

class Renderer
{
public:
	Renderer();
	Renderer(E::Shader * shader);
	Renderer(E::Shader * shader, Model * model);
	~Renderer();

	void Draw();
	void AddObject(RenderedObjectPart* objectPart);

	void Update();

	void RemoveObject(RenderedObjectPart* objectPart, bool breakAfterRemoveOnce);


	bool CheckIfTextureExist(GLuint textureHandle, int & index);


private:
	Model* model;

	std::vector<float> modelMatrix;
	E::VBO * modelMatrixVBO;

	std::vector<TextureReference> textures;
	std::vector<int*> textureIndexPtr;
	std::vector<int> textureIndex;
	E::VBO * textureIndexVBO;

	std::vector<RenderedObjectPart*> renderedObjectsPart;

	E::Shader * shader;

	int numberOfObject = 0;
	int numberOfModels = 0;
	bool modelMatrixVBONeedUpdate = false;
};

